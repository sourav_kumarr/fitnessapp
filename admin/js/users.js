/**
 * Created by Sourav on 6/13/2017.
 */

window.onload = function () {
    /*var newworkref = database.ref().child('Users').push();
    var newworkrefkey = newworkref.key;


        var creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            name : "App server",
            image:'',
            status:'A',
            type:'admin',
            email:'appserver2017@gmail.com',
            password:'App@Server17!'

        });*/

    $('#overlay').css("display","block");
    loadAllUsers();

}

function loadAllUsers() {
    var query = database.ref("Users").orderByKey();
    query.once("value").then(function(users) {
            var key='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Email</th><th>Creation Date</th><th>Login Access</th><th>Action</th>'+
                '</tr></thead><tbody>';
            var i=1;
            users.forEach(function(user) {
                console.log('users '+user.val().name);
                var obj = user.val();
                var date = getDateFromTimestamp(obj.creationDate);
                var login_access="<input data-onstyle='info' class='userstatus' data-toggle='toggle' value='"+obj.status+"'"+
                    " id='"+obj.id+"' data-size='mini' data-style='ios' type='checkbox' onchange=updateStatus('"+obj.id+"')  />";
                var image ='<i class="fa fa-2x fa-user"></i>';
                // console.log('img length -- '+obj.image.trim().length);
                if(obj.image.trim().length>0) {
                    image = '<img class="icon" src="'+obj.image+'" style="width: 50px;height:50px"/>';
                }
                if(obj.status == 'A') {
                    login_access="<input data-onstyle='info' class='userstatus'  data-toggle='toggle' value='"+obj.status+"'"+
                        "id='"+obj.id+"' data-size='mini' data-style='ios' type='checkbox' onchange=updateStatus('"+obj.id+"') checked/>";
                    // login_access = '<input type="checkbox" checked data-toggle="toggle" id="toggle-one"/>';
                }
                key = key+'<tr><td>'+i+'</td><td style="text-align: center">'+image+'</td><td>'+obj.name+'</td><td>'+obj.email+'</td><td>'+date+'</td></td><td class="buttonsTd" style="text-align: center">'+login_access+'</td>' +
                    '<td style="text-align: center"><button class="btn btn-danger" onclick=deleteUser("'+obj.id+'")><i class="fa fa-trash"></i></button></td></tr>';
                i++;
            });
            $('#overlay').css("display","none");
            $("#catTable").html(key+'</tbody>');
            $("#catTable").dataTable();
            $('#catTable').on('draw.dt', function() {
               //This will get called when data table data gets redrawn to the      table.
                 console.log('table redrawn');
                 $('.userstatus').bootstrapToggle();

             });

            $('.userstatus').bootstrapToggle();

        }).catch(function(error) {
        var errorMessage = error.message;
        // showMessage(errorMessage,"red");
    });
}

function deleteUser(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete user (y/n)?</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id) {

    var newworkref = database.ref().child('Users/'+work_id).remove().then(function(snapshot){
        loadAllUsers();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}

function updateStatus(id) {
    var newworkref = database.ref().child('Users/'+id);
    // newworkrefkey = newworkref.key;
    if($('#'+id).is(':checked')){
        newworkref.update({
            status : 'A'
        });
    }
    else{
        newworkref.update({
            status : 'D'
        });
    }

}