/**
 * Created by Sourav on 6/16/2017.
 */

/**
 * Created by Sourav on 6/13/2017.
 */

window.onload = function () {
    /*var newworkref = database.ref().child('FitnessTips').push();
     var newworkrefkey = newworkref.key;

     var message = ['Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old','Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature,',
         'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from '];
     var creation_date = Math.round((new Date()).getTime() / 1000);
     newworkref.set({
     creationDate : creation_date,
     id : newworkrefkey,
     title : "Where does it come from?",
     message:message

     });*/

    $('#overlay').css("display","block");
    loadAllTips();

}

function loadAllTips() {
    var query = database.ref("FitnessTips").orderByKey();
    query.once("value").then(function(tips) {
        var key='<thead><tr><th>S.No</th><th>title</th><th>Message</th><th>Creation Date</th><th>Action</th>'+
            '</tr></thead><tbody>';
        var i=1;
        tips.forEach(function(user) {
            var obj = user.val();
            var date = getDateFromTimestamp(obj.creationDate);
            // console.log('img length -- '+obj.image.trim().length);
            var message = obj.message[0];
            if(message.trim().length>70) {
                message = message.substr(0,70)+'...';
            }
            var tips_data = '<ol style="height: 100px;overflow: auto;list-style: decimal">';
            for(var ii in obj.message) {
                // console.log('ingre -- '+ingredients[ii]);
                tips_data = tips_data+"<li>"+obj.message[ii]+"</li>";
            }
            key = key+'<tr><td>'+i+'</td><td>'+obj.title+'</td><td title="'+message+'">'+message+"<a href='#'  data-toggle='popover' title='Tips' data-content='"+tips_data+"' data-trigger='focus' data-placement='top' data-html='true'>Click to view...</a>"+'</td><td>'+date+'</td>'+
                '<td style="text-align: center"><button class="btn btn-primary" onclick=addNewTip("'+obj.id+'")><i class="fa fa-edit"></i></button>' +
                '<button class="btn btn-danger" onclick=deleteTip("'+obj.id+'")><i class="fa fa-trash"></i></button></td></tr>';
            i++;
        });
        $('#overlay').css("display","none");
        $("#catTable").html(key+'</tbody>');
        $("#catTable").dataTable();
        $('[data-toggle="popover"]').popover();


    }).catch(function(error) {
        var errorMessage = error.message;
        showMessage(errorMessage,"red");
    });
}

function deleteTip(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this tip (y/n)?</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id) {

    var newworkref = database.ref().child('FitnessTips/'+work_id).remove().then(function(snapshot){
        loadAllTips();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}

function addNewTip(parent_id){
    if(parent_id == "0") {
        window.location = 'add_tip.php';
    }
    else{
        window.location = 'edit_tips.php?id='+parent_id;
    }
}


function updateContent(parent_id)
{

    var title = $("#title").val();
    var message = $("#message").val();

    var creation_date = Math.round((new Date()).getTime() / 1000);

    var newworkref = database.ref().child('FitnessTips').push();
    var newworkrefkey = newworkref.key;

    if(parent_id == "0") {  // add code
        if(title == "" || message == "" ){
            $("#message").html("Please fill the required fields first");
            return false;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            title : title,
            message : message
        });
        $("#myModal").modal("hide");
        loadAllTips();
    }
    else { // edit code...
        video_image = $("#selectedfilename").val();
        video_file = $("#selectedvideofile").val();

        video_name = $("#add_cat_name").val();

        if(video_image == "" || video_file == "" || video_name == '') {
            $("#message").html("Please Fill the Required Fields First");
            return false;
        }

        newworkref = database.ref().child('WorkoutVideos/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            thumbnail : video_image,
            name : video_name,
            parentId:getUrlParameter('id'),
            video:video_file
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == video_name) {
                setTimeout(function () {
                    loadWorkout_Types();
                    $("#myModal").modal("hide");

                },1000);

            }
        });


        // loadWorkouts();
    }
}
