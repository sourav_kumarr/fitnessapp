/**
 * Created by Sourav on 6/15/2017.
 */

var file_size = 0;
window.onload = function () {
    /*var newworkref = database.ref().child('Category').push();
     var newworkrefkey = newworkref.key;


     var creation_date = Math.round((new Date()).getTime() / 1000);
     newworkref.set({
     creationDate : creation_date,
     id : newworkrefkey,
     name : "Profile",
     image:'https://firebasestorage.googleapis.com/v0/b/mobigym-7cc46.appspot.com/o/images%2FSettings%20Home%20Screen.jpg?alt=media&token=24dc2aba-e54f-4ece-92a0-5e7739209822',
     status:'A'
     });*/
    // $('#overlay').css("display","none");

     loadAllCategory();

}

function loadAllCategory() {
    var query = database.ref("Category").orderByKey();
    query.once("value")
        .then(function(category) {
            var key='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Date</th><th>Action</th>'+
                '</tr></thead><tbody>';
            var i=1;
            $('#overlay').css("display","none");

            category.forEach(function(catChild) {
                var timeStamp = catChild.val().creationDate;
                var completeDate = new Date(timeStamp*1000);
                var day = completeDate.getDate();
                var month = completeDate.getMonth();
                var year = completeDate.getFullYear();
                var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                var date = day+"-"+months[month]+"-"+year;
                // if(catChild.val().name!='Profile') {
                    key = key + "<tr><td>" + i + "</td><td><img class='icon' src='" + catChild.val().image + "' style='width:50px;height:50px'/></td><td style='cursor: pointer' onclick=loadData('" + encodeURI(catChild.val().name) + "')>" +
                        catChild.val().name + "</td><td>" + date + "</td><td><i " +
                        " class='fa fa-edit' onclick=editCategory('" + catChild.val().id + "') ></i>&nbsp;" +
                        "</td></tr>";
                    i++;
                // }
            });

            $("#catTable").html(key+'</tbody>');
            $("#catTable").dataTable();
            $('#overlay').css("display","none");

        }).catch(function(error) {
        var errorMessage = error.message;
        showMessage(errorMessage,"red");
    });
}

 function editCategory(id) {
       var newworkref = database.ref().child('Category');
        newworkref.orderByKey().equalTo(id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                var name =   obj[i].name;
                var image =   obj[i].image;
                var id = obj[i].id;
                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit Category");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                    "<label>Enter Category Name</label><input type='text' id='add_cat_name' value='"+name+"'" +
                    "placeholder='Enter Category Name' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>Category Image</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><img id='selectedImage' style='display: none'/><input type='hidden' value='' id='selectedfilename' /> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' /></div>" +
                    "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit Category' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $("#selectedfilename").val(obj[i].image);
                $('#selectedImage').attr('src',obj[i].image);

            }
        });
 }

function updateContent(parent_id)
{
    var workout_image = $("#selectedfilename").val();
    var workout_name = $("#add_cat_name").val();
    var creation_date = Math.round((new Date()).getTime() / 1000);
    var file_width = $('#selectedImage').width();
    var file_height = $('#selectedImage').height();
    if(file_size>0) {
        file_size = parseInt(file_size/1000);
    }
    console.log('width -- '+file_width+' height -- '+file_height +'file size -- '+file_size);


    var newworkref = database.ref().child('Category').push();
    var newworkrefkey = newworkref.key;

    if(parent_id == "0") {   // add code
        if(workout_image == "" || workout_name == ""){
            $("#message").html("Please Fill the Required Feilds First");
            return false;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            image : workout_image,
            name : workout_name
        });
        $("#myModal").modal("hide");
        loadWorkouts();
    }
    else { // edit code...

        if(workout_image == "" || workout_name == ""){
            $("#message").html("Please Fill the Required Fields First");
            return false;
        }
        if(workout_image!="") {

            if (!(file_width == 1000) || !(file_height >= 400 || file_height <=500 )) {
                $("#message").html("Image should be 1000*(400-500) (w*h)");
                return false;

            }
            else if (file_size > 200) {
                $("#message").html("Image should not be greater than 100kb");
                return false;
            }
        }


        newworkref = database.ref().child('Category/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            image : workout_image,
            name : workout_name
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == workout_name) {
                setTimeout(function () {
                    loadAllCategory();
                    $("#myModal").modal("hide");

                },1000);

            }

        });


        // loadWorkouts();
    }
}


function setCatImage(input,showClass) {

    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        $('#selectedImage').attr('src',e.target.result);
        setTimeout(function () {
            file_size = file.size;
        },1000);

        var upload_pic = "file_"+new Date().getTime()+".jpg";
        writeDataImage(upload_pic);
        // var database = firebase.database();
        function writeDataImage(upload_pic) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: file.type
            };
            storageRef.child('images/' + upload_pic).put(file, metadata).then(function(snapshot){
                $("#addworkbtn").attr("disabled",false);

                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs) {
                        console.log('image upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedfilename").val(snapshot[i].downloadURLs);
                        $("#addworkbtn").attr("disabled",false);

                    }
                }
            });

        }
    };
    reader.readAsDataURL(file);
}

function loadData(name) {
    var data = decodeURI(name);
    if(data == 'Workouts') {
        window.location = 'index2';
    }
    else if(data == 'Diet Plans') {
        window.location = 'dietplan';
    }
    console.log(data);
}