/**
 * Created by Sourav on 6/9/2017.
 */

var orders = [];
var total_records = 0;
var file_size = 0;

function  loadWorkout_Types (){
  total_records = 0;
  var _id = getUrlParameter('id');

  // alert('id here--- '+_id);
  if(_id) {
      $("#overlay").css("display", "block");

      var newworkref = database.ref("WorkoutTypes/");

      var out_data='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Creation Date</th><th>Action</th>'+
          '</tr></thead><tbody class="sortable">';
      var counter = 1;
      var data = [];
      newworkref.orderByChild('parentId').equalTo(_id).once("value", function(snapshot) {
          var obj = snapshot.val();
          /*obj.sort(function (a,b) {
              return a.order-b.order;
          });*/
          data = [];

          for(var i in obj) {
              data.push(obj[i]);
            }

          console.log(data);

      });
       setTimeout(function () {


           if(data.length == 0) {
               $("#catTable").html("<p style='color:red'>No workout type found yet <a href='workout.php?id="+getUrlParameter('id')+"'>re-try</a></p>");
               $("#catTable").css("border","none");
               $("#overlay").css("display", "none");

               return false;
           }

           data.sort(function (a,b) {
               return a.order-b.order;
           });
           for(var i in data) {

               console.log(data);
               var completeDate = new Date(data[i].creationDate*1000);
               var day = completeDate.getDate();
               var month = completeDate.getMonth();
               var year = completeDate.getFullYear();
               var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
               var date = day+"-"+months[month]+"-"+year;
               out_data = out_data+"<tr><td id='"+data[i].id+"' >"+counter+"</td><td><img class='icon' style='width: 50px;height: 50px' src='"+data[i].image+"' /></td><td style='cursor: pointer' onclick=loadWorkoutType('"+data[i].id+"')>"+
                   data[i].name+"</td><td>"+date+"</td><td><i " +
                   " class='fa fa-edit' onclick=addNewWorkoutType('"+data[i].id+"') ></i>&nbsp;&nbsp;" +
                   "<i class='fa fa-trash' onclick=deleteWorkoutType('"+data[i].id+"')></i></td></tr>";
               $("#overlay").css("display", "none");

               counter++;
               total_records++;
           }

           out_data = out_data+'</tbody>';
           $("#catTable").html(out_data);
           $("#catTable").dataTable();
           $('.sortable').sortable({
               revert: true,
               connectWith: ".sortable",
               placeholder: "ui-state-highlight",
               helper: function(e, tr)
               {
                   var $originals = tr.children();
                   var $helper = tr.clone();
                   $helper.children().each(function(index)
                   {
                       // Set helper cell sizes to match the original sizes
                       $(this).width($originals.eq(index).width());
                   });
                   $helper.css("background-color", "rgb(223, 240, 249)");
                   return $helper;
               },
               stop: function (event, ui) {
                   console.log('order updated here----');
                   $('#catTable').find('tbody').find('tr').find('td').each(function () {

                       if ($(this).attr('id')) {
                           console.log($(this).attr('id'));
                           orders.push($(this).attr('id'));
                       }

                   });
               }
           });

       },6000);

  }
  else{
      alert('Invalid Param found');
      back();
  }
}


function addNewWorkoutType(parent_id) {
    if(parent_id == "0") {
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add New Workout");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
            "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
            "<label>Enter Workout Name</label><input type='text' id='add_cat_name' value='' " +
            "placeholder='Enter Workout Name' class='form-control' /></div></div><div class='row'>" +
            "<div class='col-md-6 form-group' ><label>Workout Image</label><input type='file'" +
            "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
            "/><input type='hidden' value='' id='selectedfilename' /> <img id='selectedImage' style='display: none'/> <div class='photobox'>" +
            "<img src='images/img.png' class='livepic img-responsive' /></div>" +
            "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
            "style='text-align:right;margin-top:40px'><input type='button' " +
            "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
            "<input type='button' value='Add WorkOut' onclick=updateContent('0') id='addworkbtn' " +
            "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show")
    }
    else{

        var newworkref = database.ref().child('WorkoutTypes');
        newworkref.orderByKey().equalTo(parent_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                var name =   obj[i].name;
                var image =   obj[i].image;
                var id = obj[i].id;
                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit Workout");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                    "<label>Enter Workout Name</label><input type='text' id='add_cat_name' value='"+name+"'" +
                    "placeholder='Enter Workout Name' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>Workout Image</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><input type='hidden' value='' id='selectedfilename' /><img id='selectedImage' style='display: none'/> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' /></div>" +
                    "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit Workout' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $("#selectedfilename").val(obj[i].image);
                $('#selectedImage').attr('src',obj[i].image);


            }
        });



    }
}


function updateContent(parent_id)
{
    var workout_image = $("#selectedfilename").val();
    var workout_name = $("#add_cat_name").val();
    var creation_date = Math.round((new Date()).getTime() / 1000);

    var newworkref = database.ref().child('WorkoutTypes').push();
    var newworkrefkey = newworkref.key;
    var file_width = $('#selectedImage').width();
    var file_height = $('#selectedImage').height();

    if(file_size>0) {
        file_size = parseInt(file_size/1000);
    }

    if(parent_id == "0") {   // add code
        if(workout_image == "" || workout_name == ""){
            $("#message").html("Please Fill the Required Feilds First");
            return false;
        }
        /*else if(!(file_width==1000 && file_height==550)) {
            $("#message").html("Image should be 1000*550 (w*h)");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300kb");
            return false;
        }
        if(total_records >0) {
            total_records++;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            image : workout_image,
            name : workout_name,
            parentId:getUrlParameter('id'),
            order:total_records

        }).then(function(snapshot){
             loadWorkout_Types();
        });
        $("#myModal").modal("hide");
        console.log('total records -- '+total_records);

    }
    else { // edit code...

        if(workout_image == "" || workout_name == ""){
            $("#message").html("Please Fill the Required Fields First");
            return false;
        }
        /*else if(!(file_width==1000 && file_height==550)) {
            $("#message").html("Image should be 1000*550 (w*h)");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300kb");
            return false;
        }

        newworkref = database.ref().child('WorkoutTypes/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            image : workout_image,
            name : workout_name
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == workout_name) {
                setTimeout(function () {
                    loadWorkout_Types();
                    $("#myModal").modal("hide");

                },1000);

            }

        });


        // loadWorkouts();
    }
}


function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function setCatImage(input,showClass) {
    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        $('#selectedImage').attr('src',e.target.result);

        setTimeout(function () {
            file_size = file.size;
        },1000);
        var upload_pic = "file_"+new Date().getTime()+".jpg";
        writeDataImage(upload_pic);
        // var database = firebase.database();
        function writeDataImage(upload_pic) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: file.type
            };
            storageRef.child('images/' + upload_pic).put(file, metadata).then(function(snapshot){
                $("#addworkbtn").attr("disabled",false);

                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs) {
                        console.log('image upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedfilename").val(snapshot[i].downloadURLs);
                        $("#addworkbtn").attr("disabled",false);

                    }
                }
            });
        }
    };
    reader.readAsDataURL(file);
}


function deleteWorkoutType(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Workout</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id){

    var newworkref = database.ref().child('WorkoutTypes/'+work_id).remove().then(function(snapshot){
        loadWorkout_Types();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}

function loadWorkoutType(id) {
    console.log('workout type loaded -- '+id);
    window.location = 'workoutdetail?id='+id;
}

function onOrderSave() {
    console.log(orders);
    if(orders.length>0) {
        for (var i = 0; i < orders.length; i++) {
            var newworkref = database.ref().child('WorkoutTypes/' + orders[i]);
            // newworkrefkey = newworkref.key;
            console.log('order id -- '+orders[i]);
            newworkref.update({
                order: i
            });

            if (i == orders.length - 1) {
                orders = [];
            }
        }
        showMessage1('Order saved successfully','Order','green');
    }
    else{
        showMessage1('No changes made yet','Order','red');

    }
}

loadWorkout_Types();