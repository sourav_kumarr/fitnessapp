// Initialize Firebase
var orders = [];
var total_records = 0;
var file_size = 0;
function loadWorkouts() {
    var query = database.ref("Workouts").orderByChild("order");
    query.once("value")
        .then(function (workout) {
            var key = '<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Date</th><th>Action</th>' +
                '</tr></thead><tbody class="sortable">';
            var i = 1;
            $('#overlay').css("display", "none");

            workout.forEach(function (workoutChild) {
                var timeStamp = workoutChild.val().creationDate;
                var completeDate = new Date(timeStamp * 1000);
                var day = completeDate.getDate();
                var month = completeDate.getMonth();
                var year = completeDate.getFullYear();
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var date = day + "-" + months[month] + "-" + year;
                key = key + "<tr><td id='" + workoutChild.val().id + "'>" + i + "</td><td><img class='icon' src='" + workoutChild.val().image + "' style='width:50px;height:50px'/></td><td style='cursor: pointer' onclick=loadWorkoutType('" + workoutChild.val().id + "')>" +
                    workoutChild.val().name + "</td><td>" + date + "</td><td><i " +
                    " class='fa fa-edit' onclick=addNewWorkout('" + workoutChild.val().id + "') ></i>&nbsp;&nbsp;&nbsp;" +
                    "<i class='fa fa-trash' onclick=deleteWorkout('" + workoutChild.val().id + "')></i></td></tr>";
                i++;
                total_records++;
            });

            key = key+'</tbody>';
            console.log(key);
            $("#catTable").html(key);
            $("#catTable").dataTable();
            $(".sortable").sortable({
                connectWith: ".sortable",
                placeholder: "ui-state-highlight",
                helper: function (e, tr) {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function (index) {
                        // Set helper cell sizes to match the original sizes
                        $(this).width($originals.eq(index).width());
                    });
                    $helper.css("background-color", "rgb(223, 240, 249)");
                    return $helper;
                },
                stop: function (event, ui) {
                    console.log('order updated here----');
                    $('#catTable').find('tbody').find('tr').find('td').each(function () {

                        if ($(this).attr('id')) {
                            console.log($(this).attr('id'));
                            orders.push($(this).attr('id'));
                        }
                    });
                }
            });

        });
}
// for category image Upload//////

function loadWorkoutType(id) {
    console.log('workout type loaded -- '+id);
    window.location = 'workout?id='+id;
}

function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function setCatImage(input,showClass) {
    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        $('#selectedImage').attr('src',e.target.result);

        setTimeout(function () {
            file_size = file.size;
        },1000);
            var upload_pic = "file_"+new Date().getTime()+".jpg";
            writeDataImage(upload_pic);
            // var database = firebase.database();
            function writeDataImage(upload_pic) {
                var storageRef = firebase.storage().ref();
                var metadata = {
                    contentType: file.type
                };
                // var uploadTask = storageRef.child('images/' + upload_pic).put(file, metadata);
                storageRef.child('images/' + upload_pic).put(file, metadata).then(function(snapshot){
                    $("#addworkbtn").attr("disabled",false);

                    for(var i in snapshot) {
                        // var download_url = snapshot[i].downloadURLs[0];

                        if(snapshot[i].downloadURLs) {
                            console.log('image upload -- '+snapshot[i].downloadURLs);
                            // console.log(snapshot[i]);
                            $("#selectedfilename").val(snapshot[i].downloadURLs);
                            $("#addworkbtn").attr("disabled",false);

                        }
                    }
                });
            }
        };
        reader.readAsDataURL(file);
}

function addNewWorkout(parent_id) {
    if(parent_id == "0") {
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add Workout Type");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
        "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
        "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
        "<label>Enter Workout Type</label><input type='text' id='add_cat_name' value='' " +
        "placeholder='Enter WorkoutType Name' class='form-control' /></div></div><div class='row'>" +
        "<div class='col-md-6 form-group' ><label>WorkoutType Image</label><input type='file'" +
        "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
        "/><input type='hidden' value='' id='selectedfilename' /><img id='selectedImage' style='display: none'/> <div class='photobox'>" +
        "<img src='images/img.png' class='livepic img-responsive' /></div>" +
        "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
        "style='text-align:right;margin-top:40px'><input type='button' " +
        "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
        "<input type='button' value='Add WorkoutType' onclick=updateContent('0') id='addworkbtn' " +
        "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show")
    }
    else{

        var newworkref = database.ref().child('Workouts');
        newworkref.orderByKey().equalTo(parent_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                 var name =   obj[i].name;
                 var image =   obj[i].image;
                 var id = obj[i].id;
                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit WorkoutType");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                    "<label>Enter WorkoutType Name</label><input type='text' id='add_cat_name' value='"+name+"'" +
                    "placeholder='Enter WorkoutType Name' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>WorkoutType Image</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><input type='hidden' value='' id='selectedfilename' /><img id='selectedImage' style='display: none'/> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' /></div>" +
                    "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit WorkoutType' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $("#selectedfilename").val(obj[i].image);
                $('#selectedImage').attr('src',obj[i].image);


            }
        });



    }
}
function editCategoryData(cat_id){
    var url = "api/categoryProcess.php";
    var options = "";
    var url2 = "api/categoryProcess.php";
    $.post(url2, {"type": "getCategory", "cat_id": cat_id}, function (data2) {
        var status2 = data2.Status;
        if (status2 == "Success") {
            var catData2 = data2.catData;
            var parent_id = catData2.parent_id;
            var level = catData2.level;
            $.post(url,{"type":"getCategory","cat_id":parent_id} ,function (data) {
                var status = data.Status;
                if (status == "Success") {
                    var catData = data.catData;
                    options = options+"<option selected='selected' value='"+catData.cat_id+";"+catData.level+"' >"+catData.cat_name+"</option>";
                    $(".modal-header").css({"display":"block"});
                    $(".modal-title").html("Edit Category");
                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                    "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Select Parent Category</label>" +
                    "<select class='form-control' id='cat_parent'>" + options + "</select>" +
                    "</div><div class='col-md-6 form-group'><label>Enter Category Name</label><input type='text'" +
                    "id='add_cat_name' value='"+catData2.cat_name+"' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>Category Display Picture</label><input type='hidden' name='type' " +
                    "value='addCategory' /><input type='file'" +
                    "id='add_cat_image' onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><div class='photobox'><img src='api/Files/images/"+catData2.cat_image+"' class='livepic img-responsive' /></div>" +
                    "</div><input type='hidden' value='editCategory' id='type' /><div class='form-group col-md-6' style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Category' " +
                    "onclick=addContent('"+cat_id+"') class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                    $(".modal-footer").css({"display":"none"});
                    $("#myModal").modal("show");
                }
            });
        }
    });
}
function changeApproval(approval_id,approval_status) {
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"changeApproval",'approval_id':approval_id,'approval_status':approval_status} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function logout(){
   window.location = 'logout.php';
}
function updateContent(parent_id)
{
    var workout_image = $("#selectedfilename").val();
    var workout_name = $("#add_cat_name").val();
    var creation_date = Math.round((new Date()).getTime() / 1000);
    var file_width = $('#selectedImage').width();
    var file_height = $('#selectedImage').height();

    if(file_size>0) {
        file_size = parseInt(file_size/1000);
    }
    console.log('width -- '+file_width+' height -- '+file_height +'file size -- '+file_size);

    var newworkref = database.ref().child('Workouts').push();
    var newworkrefkey = newworkref.key;

    if(parent_id == "0") {   // add code
        if(workout_image == "" || workout_name == ""){
            $("#message").html("Please Fill the Required Feilds First");
            return false;
        }
        /*else if(!(file_width==1000) && !(file_height >= 400 || file_height <=500)) {
            $("#message").html("Image should be 100*(400-500 (w*h))");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300kb");
            return false;
        }
        if(total_records>0) {
            total_records++;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            image : workout_image,
            name : workout_name,
            order:total_records

        });
        $("#myModal").modal("hide");
        loadWorkouts();
    }
    else { // edit code...

        if(workout_image == "" || workout_name == "") {
            $("#message").html("Please Fill the Required Fields First");
            return false;
        }

        /*else if(!(file_width==1000) || !(file_height >= 400 || file_height <=500)) {
            $("#message").html("Image should be 1000*(400-500) (w*h)");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300kb");
            return false;
        }
        $('#selectedImage').attr('src','');
        newworkref = database.ref().child('Workouts/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            image : workout_image,
            name : workout_name
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == workout_name) {
                setTimeout(function () {
                     loadWorkouts();
                    $("#myModal").modal("hide");

                },1000);
            }

        });


        // loadWorkouts();
    }
}


function deleteWorkout(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Workout</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}

function confirmDelete(work_id){

    var newworkref = database.ref().child('Workouts/'+work_id).remove().then(function(snapshot){
        loadWorkouts();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}
//////////////////////////////////////////////////////////////////////////////
///category end
//////////////////////////////////////////////////////////////////////////////
function addNewProduct(parent_id){
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategory","cat_id":parent_id} ,function(data) {
        var status = data.Status;
        var catShow = "";
        if (status == "Success") {
            var mainCategory = "";
            var categoryData = data.catData;
            mainCategory = data.TopParentName;
            catShow += "<option value='" + categoryData.cat_id + "' >" + categoryData.cat_name + "</option>";
            $(".modal-header").css({"display": "block"});
            $(".modal-title").html("Add New Product");
            $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
            "</label></div><br><div class='row'><div class='form-group col-md-6'><label>Parent Category</label>" +
            "<select id='parent_category' class='form-control'>" + catShow + "</select></div><div class='col-md-6 form-group'>" +
            "<label>Product Name</label><input type='text' id='product_name' value='' placeholder='Enter Product Name' " +
            "class='form-control' /></div></div><div class='row'><div class='col-md-6 form-group' >" +
            "<label>Product Price</label><input type='text' class='form-control' value='' placeholder='Enter Product Price'" +
            " id='product_price' /></div><div class='form-group col-md-6'><label>Select Product File</label>" +
            "<input type='file' id='product_file' style='width:100%' /></div></div><div class='row'>" +
            "<div class='col-md-6 form-group'><label>Book Description</label><textarea id='product_desc' " +
            "placeholder='Enter Product Description' class='form-control' ></textarea></div><div class='form-group col-md-3'>" +
            "<label>Product Image</label><input type='file' id='image_file' onchange=setPhoto(this,'livepic') " +
            "style='position: absolute;width:58px;height:60px;opacity:0' /><div class='photobox'><img src='images/img.png'" +
            " class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
            "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='hidden' value='addProduct' id='type'>" +
            "<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp;" +
            "<input type='button' value='Add New Product' class='btn btn-info formbtn' onclick=productProcess('','"+escape(mainCategory)+"') />" +
            "</div></div><img src='images/default.gif' class='loadNow' />");
            $(".modal-footer").css({"display": "none"});
            $("#myModal").modal("show");
        }
    });
}
function editProductData(product_id) {
    var url = "api/productProcess.php";
    $.post(url, {"type": "getParticularProductData", "product_id": product_id}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            var productData = data.ProductData;
            var cat_id = productData.category_id;
            var url = "api/categoryProcess.php";
            $.post(url, {"type": "getCategory","cat_id":cat_id}, function (data) {
                var status = data.Status;
                var catShow = "";
                var mainCategory = "";
                if (status == "Success") {
                    var categoryData = data.catData;
                    mainCategory = data.TopParentName;
                    catShow += "<option value='" + categoryData.cat_id + "' selected='selected' >" + categoryData.cat_name + "</option>";
                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Edit Product Detail");
                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'>" +
                    "</p><div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                    "<label>Selected Category</label><select id='parent_category' class='form-control'>"+catShow+
                    "</select></div><div class='col-md-6 form-group'>" +
                    "<label>Product Name</label><input type='text' id='product_name' value='"+productData.product_name+
                    "' placeholder='Enter Product Name' class='form-control' /></div></div><div class='row'>" +
                    "<div class='col-md-6 form-group'><label>Product Price</label><input type='text' " +
                    "class='form-control' id='product_price' value='" + productData.product_price+"' " +
                    "placeholder='Enter Product Price'/></div><div class='col-md-6 form-group'><label>Selected Music" +
                    " File</label><input type='file' id='product_file' style='margin-top:3px;width:100%;display:none' />" +
                    "<p style='line-height:30px' id='oldProductFile'>"+productData.file_name+" <i class='fa fa-edit' " +
                    "onclick=editproductFile() style='cursor:pointer'></i></p></div></div><div class='row'>" +
                    "<div class='col-md-6'><label>Product Description</label><textarea class='form-control' id='product_desc'>"+
                    productData.product_desc+"</textarea></div><div class='form-group col-md-6'>" +
                    "<label>Product Image</label><input type='file' id='image_file' " +
                    "onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0'" +
                    " /><div class='photobox'><img src='api/Files/images/"+productData.product_image+"' " +
                    "class='livepic img-responsive' /></div></div></div><div class='row'><div class='form-group " +
                    "col-md-6 pull-right' style='text-align:right;margin-top:40px'><input type='button' value='Cancel' " +
                    "data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Product'" +
                    "class='btn btn-info formbtn' onclick=productProcess('"+product_id+"','"+escape(mainCategory)+"') /></div></div>" +
                    "<img src='images/default.gif' class='loadNow' /><input type='hidden' value='editProduct' id='type' />");
                    $(".modal-footer").css({"display": "none"});
                    $("#myModal").modal("show");
                }
            });
        }
    });
}
function editproductFile(){
    $("#oldProductFile").hide();
    $("#product_file").show();
}
function playVideoSong(songfile) {
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"none"});
    $(".modal-footer").css({"display":"none"});
    $(".modal-dialog").html("<video class='playsong' style='margin:20% 0 0 9%' controls autoplay>" +
    "<source src='api/Files/video/"+songfile+"' /></video>" +
    "<i class='fa fa-close fa-2x' onclick='stopMusic()' style='position:absolute;color:white;cursor:pointer;top:96px'></i>");
    $("#myModal").modal("show");
}
function stopMusic(){
    $(".playsong").remove();
    $("#myModal").modal("hide");
    setTimeout(function () {
        $(".modal-dialog").html("<div class='modal-content'><div class='modal-header'><button type='button' class='close' " +
        "data-dismiss='modal'>&times;</button><h4 class='modal-title'></h4></div><div class='modal-body'>" +
        "</div><div class='modal-footer'></div></div>");
    },300);
}

 function onOrderSave() {
    console.log(orders);
    if(orders.length>0) {
        for (var i = 0; i < orders.length; i++) {
            var newworkref = database.ref().child('Workouts/' + orders[i]);
            // newworkrefkey = newworkref.key;

            newworkref.update({
                order: i
            });

            if (i == orders.length - 1) {
                orders = [];
            }
        }
        showMessage1('Order saved successfully','Order','green');
    }
    else{
        showMessage1('No changes made yet','Order','red');

    }
 }





