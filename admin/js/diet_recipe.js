/**
 * Created by Sourav on 6/12/2017.
 */

/**
 * Created by Sourav on 6/10/2017.
 */

/**
 * Created by Sourav on 6/10/2017.
 */

// Initialize Firebase


var orders = [];
function  loadAllDiet_recipe (){
    var _id = getUrlParameter('id');

    // alert('id here--- '+_id);
    if(_id) {
        var image = '';
        var newworkref0 = database.ref().child('Diets');
        newworkref0.orderByChild('id').equalTo(_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj) {
              image = obj[i].image;
              console.log('image -- '+image);
           }
        });

        var newworkref = database.ref().child('DietRecipe');

        var counter = 1;

        var out_data='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Directions</th><th>Ingredients</th><th>Creation Date</th><th>Action</th>'+
            '</tr></thead><tbody class="sortable">';
        var data = [];
        newworkref.orderByChild('dietId').equalTo(_id).once("value", function(snapshot) {
            var obj = snapshot.val();
            /*obj.sort(function (a,b) {
             return a.order-b.order;
             });*/
            data = [];

            for(var i in obj) {
                data.push(obj[i]);
            }

            console.log(data);

        });
        setTimeout(function () {


            if(data.length == 0) {
                $("#catTable").html("<p style='color:red'>No workout type found yet <a href='recipe?id="+getUrlParameter('id')+"' style='text-decoration: underline'>re-try</a></p>");
                $("#catTable").css("border","none");
                $("#overlay").css("display", "none");

                return false;
            }

            data.sort(function (a,b) {
                return a.order-b.order;
            });
            for(var i in data) {
                var completeDate = new Date(data[i].creationDate*1000);
                var day = completeDate.getDate();
                var month = completeDate.getMonth();
                var year = completeDate.getFullYear();
                var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                var date = day+"-"+months[month]+"-"+year;
                var ingredients = data[i].Ingredients;
                var directions = data[i].Directions;

                // ingrediants data ...

                var ingredients_data = '<ol style="height: 100px;overflow: auto;list-style: disc">';
                for(var ii in ingredients) {
                    // console.log('ingre -- '+ingredients[ii]);
                    ingredients_data = ingredients_data+"<li>"+ingredients[ii]+"</li>";
                }
                ingredients_data =ingredients_data+ '</ol>';

                // ingrediants data ...

                var directions_data = '<ol style="height: 100px;overflow: auto;list-style: decimal">';
                for(var ii in directions) {
                    // console.log('ingre -- '+directions[ii]);
                    directions_data = directions_data+"<li>"+directions[ii]+"</li>";
                }
                directions_data =directions_data+ '</ol>';


                out_data = out_data+"<td id='"+data[i].id+"'>"+counter+"</td><td class='image_i'><img src='"+image+"' class='icon' style='width: 50px;height: 50px'/></td><td style='cursor: pointer' ><a href='view_recipe.php?resp="+getUrlParameter('id')+"&id="+data[i].id+"' >"+
                    data[i].name+"</a></td><td style='width:28%'>"+directions[0]+" <a href='view_recipe.php?resp="+getUrlParameter('id')+"&id="+data[i].id+"'  data-toggle='popover' title='Directions' data-content='"+directions_data+"' data-trigger='focus' data-placement='top' data-html='true'>Click to view...</a>" +
                    "</td><td style='width:28%'>"+ingredients[0]+" <a href='#'  onclick=addNewRecipe('"+data[i].id+"') data-toggle='popover' title='Directions' data-content='"+ingredients_data+"' data-trigger='focus' data-placement='top' data-html='true'>Click to view...</a></td><td>"+date+"</td><td><i " +
                    " class='fa fa-edit' onclick=addNewRecipe('"+data[i].id+"') ></i>&nbsp;&nbsp;" +
                    "<i class='fa fa-trash' onclick=deleteWorkout('"+data[i].id+"')></i></td></tr>";
                counter++;
            }

            $("#overlay").css("display","none");
            out_data = out_data+'</tbody>';
            $("#catTable").html(out_data);
            $("#catTable").dataTable();

            $('.sortable').sortable({
                revert: true,
                connectWith: ".sortable",
                placeholder: "ui-state-highlight",
                helper: function(e, tr)
                {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                        // Set helper cell sizes to match the original sizes
                        $(this).width($originals.eq(index).width());
                    });
                    $helper.css("background-color", "rgb(223, 240, 249)");
                    return $helper;
                },
                stop: function (event, ui) {
                    console.log('order updated here----');
                    $('#catTable').find('tbody').find('tr').find('td').each(function () {

                        if ($(this).attr('id')) {
                            console.log($(this).attr('id'));
                            orders.push($(this).attr('id'));
                        }

                    });
                }
            });

            $('[data-toggle="popover"]').popover();



        },6000);

        /*newworkref.orderByChild('dietId').equalTo(_id).on("value", function(snapshot) {
            var obj = snapshot.val();
            // console.log(JSON.stringify(obj));
            var out_data='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Directions</th><th>Ingredients</th><th>Creation Date</th><th>Action</th>'+
                '</tr></thead>';
            var counter = 1;
            for(var i in obj) {
                // console.log(obj[i].name);
                var completeDate = new Date(obj[i].creationDate*1000);
                var day = completeDate.getDate();
                var month = completeDate.getMonth();
                var year = completeDate.getFullYear();
                var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                var date = day+"-"+months[month]+"-"+year;
                var ingredients = obj[i].Ingredients;
                var directions = obj[i].Directions;

                // ingrediants data ...

                var ingredients_data = '<ol style="height: 100px;overflow: auto;list-style: disc">';
                for(var ii in ingredients) {
                    // console.log('ingre -- '+ingredients[ii]);
                    ingredients_data = ingredients_data+"<li>"+ingredients[ii]+"</li>";
                }
                ingredients_data =ingredients_data+ '</ol>';

                // ingrediants data ...

                var directions_data = '<ol style="height: 100px;overflow: auto;list-style: decimal">';
                for(var ii in directions) {
                    // console.log('ingre -- '+directions[ii]);
                    directions_data = directions_data+"<li>"+directions[ii]+"</li>";
                }
                directions_data =directions_data+ '</ol>';


                out_data = out_data+"<tbody><td>"+counter+"</td><td class='image_i'><img src='"+image+"' class='icon' style='width: 50px;height: 50px'/></td><td style='cursor: pointer' >"+
                    obj[i].name+"</td><td style='width:28%'>"+directions[0]+" <a href='#' data-toggle='popover' title='Directions' data-content='"+directions_data+"' data-trigger='focus' data-placement='top' data-html='true'>Click to view...</a>" +
                    "</td><td style='width:28%'>"+ingredients[0]+" <a href='#' href='#' data-toggle='popover' title='Directions' data-content='"+ingredients_data+"' data-trigger='focus' data-placement='top' data-html='true'>Click to view...</a></td><td>"+date+"</td><td><i " +
                    " class='fa fa-edit' onclick=addNewRecipe('"+obj[i].id+"') ></i>&nbsp;" +
                    "<i class='fa fa-trash' onclick=deleteWorkout('"+obj[i].id+"')></i></td></tr></tbody>";
                counter++;
            }
            $("#overlay").css("display","none");
            $("#catTable").html(out_data);
            $("#catTable").dataTable();
            $('[data-toggle="popover"]').popover();

        });*/
    }
    else{
        alert('Invalid Param found');
        back();
    }

}

function loadWorkoutType(id) {
    console.log('workout type loaded -- '+id);
    // window.location = 'diets?id='+id;
}

function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function setCatImage(input,showClass) {
    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        var upload_pic = "file_"+new Date().getTime()+".jpg";
        writeDataImage(upload_pic);
        // var database = firebase.database();
        function writeDataImage(upload_pic) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: file.type
            };
            var uploadTask = storageRef.child('images/' + upload_pic).put(file, metadata);
            var starsRef = storageRef.child('images/'+upload_pic);
            setTimeout(function(){
                starsRef.getDownloadURL().then(function(snapshot){
                    console.log(snapshot);
                    $("#selectedfilename").val(snapshot);
                }).catch(function (error) {
                    switch (error.code) {
                        case 'storage/object_not_found':
                            break;
                        case 'storage/unauthorized':
                            break;
                        case 'storage/canceled':
                            break;
                        case 'storage/unknown':
                            break;
                    }
                    $("#message").html(error.code);
                    $("#myModal").modal("hide");
                });
                $("#addworkbtn").attr("disabled",false);
            },3000);
        }
    };
    reader.readAsDataURL(file);
}

function addNewRecipe(parent_id) {
    if(parent_id == "0") {
        window.location = "add_recipe.php?id="+getUrlParameter('id');
        /*$(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add Recipe");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
            "<label class='error'></label></div><br><div class='row'><div class='col-md-6' >" +
            "<label>Recipe Name</label><input type='text' id='add_cat_name' value='' " +
            "placeholder='Enter Diet Name' class='form-control' /></div> " +
            "</div></div> <div class='row'>" +
            "<input type='hidden' value='addCategory' id='type' /> <div class='col-md-6 form-group' ><label>Diet Image</label><input type='file'" +
            "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
            "/><input type='hidden' value='' id='selectedfilename' /> <div class='photobox'>" +
            "<img src='images/img.png' class='livepic img-responsive' /></div></div><div class='form-group col-md-6 pull-right ' " +
            "style='text-align:right;margin-top:40px'><input type='button' " +
            "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
            "<input type='button' value='Add DietPlan' onclick=updateContent('0') id='addworkbtn' " +
            "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show");*/
    }
    else{

        /*var newworkref = database.ref().child('Diets');
        newworkref.orderByKey().equalTo(parent_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                var name =   obj[i].name;
                var image =   obj[i].image;
                var id = obj[i].id;

                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit DietPlan");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6' >" +
                    "<label>Diet Name</label><input type='text' id='add_cat_name' value='"+name+"' " +
                    "placeholder='Enter Diet Name' class='form-control' /></div> " +
                    "</div></div> <div class='row'>" +
                    "<input type='hidden' value='addCategory' id='type' /> <div class='col-md-6 form-group' ><label>Diet Image</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><input type='hidden' value='' id='selectedfilename' /> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' /></div></div><div class='form-group col-md-6 pull-right ' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit Diet' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $("#selectedfilename").val(image);

            }
        });
      */
        window.location = "edit_recipe.php?resp="+getUrlParameter('id')+"&id="+parent_id;

    }
}



function deleteWorkout(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Recipe</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id){

    database.ref().child('DietRecipe/'+work_id).remove().then(function(snapshot){
        loadAllDiet_recipe();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}

function onOrderSave() {
    console.log(orders);
    if(orders.length>0) {
        for (var i = 0; i < orders.length; i++) {
            var newworkref = database.ref().child('DietRecipe/' + orders[i]);
            // newworkrefkey = newworkref.key;

            newworkref.update({
                order: i
            });

            if (i == orders.length - 1) {
                orders = [];
            }
        }
        showMessage1('Order saved successfully','Order','green');
    }
    else{
        showMessage1('No changes made yet','Order','red');

    }
}

loadAllDiet_recipe();


