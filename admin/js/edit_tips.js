/**
 * Created by Sourav on 6/12/2017.
 */

/**
 * Created by Sourav on 6/12/2017.
 */
var recipe = {}
window.onload = function () {

    loadRecipe();
}


function loadRecipe() {
    $("#overlay").css('display','block');
    recipe = new Recipe();

    var newworkref = database.ref().child('FitnessTips');
    newworkref.orderByKey().equalTo(getUrlParameter("id")).on("value", function(snapshot) {
        // console.log(Object.keys(snapshot.val())[0].id);
        var obj = snapshot.val();
        for (var i in obj) {
            var name = obj[i].title;
            var id = obj[i].id;
            var message = obj[i].message;


            // $('#selectedvideofile').val(video);
            // $('#selectedvideofile1').val(video);
            // $('#file_txt').val(video);
            // $('#file_txt').attr('title',video);
            $('#title').val(name);
            loadTextBoxes(message);
        }
        $("#overlay").css('display','none');
    });
}

function loadTextBoxes(ingredients) {
    var ingre_data = '';
    for(var i=0;i<ingredients.length;i++) {
        ingre_data = ingre_data+'<li style="margin-top: 5px;width: 89%" id="ingredients_'+i+'"><input type="text"  class="form-control" placeholder="Enter ingredients detail" id="ingredients_'+i+'s" value="'+ingredients[i]+'"></li>'+
            '<li id="remove_ingredients_'+i+'"><button class="btn btn-danger" onclick=onRemoveItem("ingredients",'+i+')><i class="fa fa-trash"></i></button></li>'
        recipe.addRecipeItem(ingre_data,'ingredients');
    }
    $('#ingredients_data').html(ingre_data);

}

function onAddItem(type) {

    var data = recipe.getRecipeItem(type);
    console.log(data);
    var index = 0;
    if(data.length>0) {
        index = data.length-1;
    }
    index++;
    recipe.addRecipeItem('<li style="margin-top: 5px;width: 89%" id="'+type+'_'+index+'"><input type="text"  class="form-control" placeholder="Enter ingredients detail" id="'+type+'_'+index+'s"></li>'+
        '<li id="remove_'+type+'_'+index+'"><button class="btn btn-danger" onclick=onRemoveItem("'+type+'",'+index+')><i class="fa fa-trash"></i></button></li>',type);
    var list = '';
    /*for(var i=0;i<data.length;i++) {
     list = list+data[i];
     }*/
    var height = $('#'+type+'_data').height();

    $('#'+type+'_data').append('<li style="margin-top: 5px;width: 89%" id="'+type+'_'+index+'"><input type="text"  class="form-control" placeholder="Enter ingredients detail" id="'+type+'_'+index+'s"></li>'+
        '<li id="remove_'+type+'_'+index+'"><button class="btn btn-danger" onclick=onRemoveItem("'+type+'",'+index+')><i class="fa fa-trash"></i></button></li>');

    $('#'+type+'_data').animate({scrollTop:'999px'}, 'slow');
}

function onRemoveItem(type,index) {
    var data = recipe.getRecipeItem(type);
    if(data.length>1) {
        recipe.removeRecipeItem(index,type);
        $('#'+type+'_'+index).remove();
        $('#remove_'+type+'_'+index).remove();
    }

}

function onSaveItem() {
    var title = $('#title').val();
    var ingredients = validateData('ingredients');
    console.log(ingredients);

    if(title=='' || !(Array.isArray(ingredients))) {
        $('#message').html('please enter all fields');
        $('#message').css('color','red');
    }
    else {
        var creation_date = Math.round((new Date()).getTime() / 1000);

        var newworkref = database.ref().child('FitnessTips/'+getUrlParameter("id"));
        // newworkrefkey = newworkref.key;

        newworkref.update({
            title : title,
            message:ingredients
        });
        $('#message').html('Updated successfully');
        $('#message').css('color','green');

        // window.location = 'recipe.php?id='+getUrlParameter('resp');
        // location.reload();
        loadRecipe();

    }

}

function onFileSelected(idd) {
    if(idd == 'filepath') {
        $('#filepath').css('display','none');
        $('#filepath1').css('display','block');

    }
    else{
        $('#filepath').css('display','block');
        $('#filepath1').css('display','none');
        $('#selectedvideofile').val($('#selectedvideofile1').val());
    }

}

function validateData(type) {
    var items = recipe.getRecipeItem(type);
    var item_data = [];
    if(items.length>0) {
        // for(var i=0;i<items.length;i++) {
            var childeren = $('#'+type+'_data').children().length;
            console.log('childeren --- '+childeren);
            var value = '';

            for(var ii=0;ii<childeren;ii++) {
                var id = $('ul#'+type+'_data li').eq(ii).attr('id');
                var idd = id.split('_');
                if(idd.length == 2) {
                    value = $('#'+id+'s').val();
                    if(value!=''){
                        item_data.push(value);
                    }
                    else{
                        return false;
                    }
                }
                // console.log('id -- '+$('ul#'+type+'_data li').eq(ii).attr('id'));

            // }

            // var value = 'undefined';
            console.log('value -- '+value);

        }
        return item_data;
    }

    return false;
}

function setCatImage1(input) {

    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        // $('.'+showClass).attr('src', e.target.result);
        var file_name = file.name;
        var ext = file_name.substr(file_name.lastIndexOf(".")+1,file_name.length);
        console.log(ext);
        if(!checkVideoType(ext.toUpperCase())) {
            $('#message').html("please select these "+files.join(",") +" video files");
            $('#message').css('color','red');
            return ;
        }
        $("#save_btn").attr("disabled",true);

        var upload_video = "video_"+new Date().getTime()+"."+ext.toLowerCase();
        $('.loadNow').css('display','block');
        writeDataImage(upload_video);
        // var database = firebase.database();
        function writeDataImage(upload_video) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: ext
            };
            storageRef.child('videos/' + upload_video).put(file, metadata).then(function (snapshot) {
                $('.loadNow').css('display','none');
                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs){
                        console.log('video upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedvideofile").val(snapshot[i].downloadURLs);
                        $("#save_btn").attr("disabled",false);

                    }
                }
            });
        }
    };
    reader.readAsDataURL(file);
}


function Recipe() {
    this.ingredients = [];
    this.directions = [];
    this.addRecipeItem = function (item,type) {
        if(type == 'ingredients')
            this.ingredients.push(item);
        else if(type == 'directions')
            this.directions.push(item);
    }

    this.removeRecipeItem = function (type,index) {

        if(type == 'ingredients')
            this.ingredients.splice(index,1);
        else if(type == 'directions')
            this.directions.splice(index,1);




    }
    this.getRecipeItem = function (type) {
        if(type == 'ingredients')
            return this.ingredients;
        else if(type == 'directions')
            return this.directions;

    }

}