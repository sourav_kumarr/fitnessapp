/**
 * Created by Sourav on 6/10/2017.
 */

// Initialize Firebase

var orders = [];
var total_records = 0;
var file_size = 0;

function loadDietPlans()
{
    var query = database.ref("DietPlan").orderByChild('order');
    query.once("value")
        .then(function(workout) {
            var key='<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Description</th><th>Creation Date</th><th>Action</th>'+
                '</tr></thead><tbody class="sortable">';
            var i=1;
            $('#overlay').css("display","none");

            workout.forEach(function(workoutChild) {
                var timeStamp = workoutChild.val().creationDate;
                var completeDate = new Date(timeStamp*1000);
                var day = completeDate.getDate();
                var month = completeDate.getMonth();
                var year = completeDate.getFullYear();
                var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                var date = day+"-"+months[month]+"-"+year;
                key = key+"<tr><td id='"+workoutChild.val().id+"'>"+i+"</td><td><img class='icon' src='"+workoutChild.val().image+"' style='width:50px;height:50px'/></td><td style='cursor: pointer' onclick=loadWorkoutType('"+workoutChild.val().id+"')>"+
                    workoutChild.val().name+"</td><td style='width:46%'>"+workoutChild.val().headerMessage+"</td><td>"+date+"</td><td><i " +
                    " class='fa fa-edit' onclick=addNewWorkout('"+workoutChild.val().id+"') ></i>&nbsp;&nbsp;" +
                    "<i class='fa fa-trash' onclick=deleteWorkout('"+workoutChild.val().id+"')></i></td></tr>";
                i++;
                total_records++;
            });

            key = key+'</tbody>';
            $("#catTable").html(key);
            $("#catTable").dataTable();

            $('.sortable').sortable({
                revert: true,
                connectWith: ".sortable",
                placeholder: "ui-state-highlight",
                helper: function(e, tr)
                {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                        // Set helper cell sizes to match the original sizes
                        $(this).width($originals.eq(index).width());
                    });
                    $helper.css("background-color", "rgb(223, 240, 249)");
                    return $helper;
                },
                stop : function(event,ui){
                    console.log('order updated here----');
                    $('#catTable').find('tbody').find('tr').find('td').each(function(){

                        if($(this).attr('id')) {
                            console.log($(this).attr('id'));
                            orders.push($(this).attr('id'));
                        }

                    })
                }
            });

        }).catch(function(error) {
        var errorMessage = error.message;
        showMessage(errorMessage,"red");
    });
}

function loadWorkoutType(id) {
    console.log('workout type loaded -- '+id);
    window.location = 'diets?id='+id;
}

function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function setCatImage(input,showClass) {
    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        $('#selectedImage').attr('src',e.target.result);

        setTimeout(function () {
            file_size = file.size;
        },1000);

        var upload_pic = "file_"+new Date().getTime()+".jpg";
        writeDataImage(upload_pic);
        // var database = firebase.database();
        function writeDataImage(upload_pic) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: file.type
            };
            storageRef.child('images/' + upload_pic).put(file, metadata).then(function(snapshot){
                $("#addworkbtn").attr("disabled",false);

                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs) {
                        console.log('image upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedfilename").val(snapshot[i].downloadURLs);
                        $("#addworkbtn").attr("disabled",false);

                    }
                }
            });
        }
    };
    reader.readAsDataURL(file);
}

function addNewWorkout(parent_id) {
    if(parent_id == "0") {
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add Diet Plan");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
            "<label class='error'></label></div><br><div class='row'><div class='col-md-6' ><div class='col-md-12 form-group'>" +
            "<label>Diet Name</label><input type='text' id='add_cat_name' value='' " +
            "placeholder='Enter Diet Name' class='form-control' /></div><div class='col-md-12 form-group' ><label>Category Image</label><input type='file'" +
            "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
            "/><input type='hidden' value='' id='selectedfilename' /> <img id='selectedImage' style='display: none'/> <div class='photobox'>" +
            "<img src='images/img.png' class='livepic img-responsive' /></div>" +
            "</div></div>" +
            "<div class='col-md-6 form-group'><label>Diet Description</label><textarea rows='4' cols='50' style='width:100%;resize: none' id='desc'/></div></div> <div class='row'>" +
            "<input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-12 pull-right ' " +
            "style='text-align:right;margin-top:40px'><input type='button' " +
            "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
            "<input type='button' value='Add DietPlan' onclick=updateContent('0') id='addworkbtn' " +
            "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show")
    }
    else{

        var newworkref = database.ref().child('DietPlan');
        newworkref.orderByKey().equalTo(parent_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                var name =   obj[i].name;
                var image =   obj[i].image;
                var desc = obj[i].headerMessage;
                var id = obj[i].id;
                $('#selectedfilename').val(image);

                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit DietPlan");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6' ><div class='col-md-12 form-group'>" +
                    "<label>Diet Name</label><input type='text' id='add_cat_name' value='"+name+"' " +
                    "placeholder='Enter Diet Name' class='form-control' /></div><div class='col-md-12 form-group' ><label>Category Image</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><input type='hidden' value='' id='selectedfilename' /> <img id='selectedImage' style='display: none'/> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' /></div>" +
                    "</div></div>" +
                    "<div class='col-md-6 form-group'><label>Diet Description</label><textarea rows='4' cols='50' style='width:100%;resize: none' id='desc' >"+desc+"</textarea></div></div> <div class='row'>" +
                    "<input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-12 pull-right ' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit DietPlan' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    "class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $("#selectedfilename").val(obj[i].image);
                $('#selectedImage').attr('src',image);


            }
        });



    }
}
function updateContent(parent_id)
{
    var diet_image = $("#selectedfilename").val();
    var diet_name = $("#add_cat_name").val();
    var creation_date = Math.round((new Date()).getTime() / 1000);
    var diet_desc = $('#desc').val();

    var newworkref = database.ref().child('DietPlan').push();
    var newworkrefkey = newworkref.key;

    var file_width = $('#selectedImage').width();
    var file_height = $('#selectedImage').height();

    if(file_size>0) {
        file_size = parseInt(file_size/1000);
    }

    console.log('diet image -- '+diet_image);

    if(parent_id == "0") {   // add code
        if(diet_image == "" || diet_name == "" || diet_desc == "") {
            $("#message").html("Please Fill the Required Feilds First");
            return false;
        }
        /*else if(!(file_width==512 && file_height==512)) {
            $("#message").html("Image should be 512*512 (w*h)");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300 kb");
            return false;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            image : diet_image,
            name : diet_name,
            headerMessage:diet_desc,
            numOfRecipe:0,
            order:total_records
        });
        $("#myModal").modal("hide");
        loadDietPlans();
    }
    else { // edit code...

        if(diet_image == "" || diet_name == "" || diet_desc == "") {
            $("#message").html("Please Fill the Required Feilds First");
            return false;
        }
        /*else if(!(file_width==512 && file_height==512)) {
            $("#message").html("Image should be 512*512 (w*h)");
            return false;

        }*/
        else if(file_size>300) {
            $("#message").html("Image should not be greater than 300kb");
            return false;
        }
        newworkref = database.ref().child('DietPlan/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            image : diet_image,
            name : diet_name,
            headerMessage:diet_desc
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == diet_name) {
                setTimeout(function () {
                    loadDietPlans();
                    $("#myModal").modal("hide");

                },1000);

            }

        });


        // loadWorkouts();
    }
}


function deleteWorkout(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Diet Plan</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id){

    var newworkref = database.ref().child('DietPlan/'+work_id).remove().then(function(snapshot){
        loadDietPlans();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}
function onOrderSave() {
    console.log(orders);
    if(orders.length>0) {
        for (var i = 0; i < orders.length; i++) {
            var newworkref = database.ref().child('DietPlan/' + orders[i]);
            // newworkrefkey = newworkref.key;

            newworkref.update({
                order: i
            });

            if (i == orders.length - 1) {
                orders = [];
            }
        }
        showMessage1('Order saved successfully','Order','green');
    }
    else{
        showMessage1('No changes made yet','Order','red');

    }
}
loadDietPlans();


