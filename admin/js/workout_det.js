/**
 * Created by Sourav on 6/9/2017.
 */

var source = document.createElement('source');
var VideoSnapper = {

    /**
     * Capture screen as canvas
     * @param {HTMLElement} video element
     * @param {Object} options = width of screen, height of screen, time to seek
     * @param {Function} handle function with canvas element in param
     */
    captureAsCanvas: function(video, options, handle) {

        // Create canvas and call handle function
        var callback = function() {
            // Create canvas
            var canvas = $('#canvas').attr({
                width: options.width,
                height: options.height
            })[0];
            // Get context and draw screen on it
            canvas.getContext('2d').drawImage(video, 0, 0, options.width, options.height);
            // Seek video back if we have previous position
            if (prevPos) {
                // Unbind seeked event - against loop
                $(video).unbind('seeked');
                // Seek video to previous position
                video.currentTime = prevPos;
            }
            // Call handle function (because of event)
            handle.call(this, canvas);
        }

        // If we have time in options
        if (options.time && !isNaN(parseInt(options.time))) {
            // Save previous (current) video position
            var prevPos = video.currentTime;
            // Seek to any other time
            video.currentTime = options.time;
            // Wait for seeked event
            $(video).bind('seeked', callback);
            return;
        }

        // Otherwise callback with video context - just for compatibility with calling in the seeked event
        return callback.apply(video);
    }
};


var orders = [];
var total_records = 0;

function  loadWorkout_Types (){
    var _id = getUrlParameter('id');

    // alert('id here--- '+_id);
    if(_id) {
        $("#overlay").css("display", "block");
        var newworkref = database.ref().child('WorkoutVideos');
        var counter = 1;

        var out_data = '<thead><tr><th>S.No</th><th>Image</th><th>Name</th><th>Creation Date</th><th>Action</th>' +
            '</tr></thead><tbody class="sortable">';
        var data = [];
        newworkref.orderByChild('parentId').equalTo(_id).once("value", function(snapshot) {
            var obj = snapshot.val();
            /*obj.sort(function (a,b) {
             return a.order-b.order;
             });*/
            data = [];

            for(var i in obj) {
                data.push(obj[i]);
            }

            console.log(data);

        });
        setTimeout(function () {


            if(data.length == 0) {
                $("#catTable").html("<p style='color:red'>No workout type found yet <a href='workoutdetail?id="+getUrlParameter('id')+"' style='text-decoration: underline'>re-try</a></p>");
                $("#catTable").css("border","none");
                $("#overlay").css("display", "none");

                return false;
            }

            data.sort(function (a,b) {
                return a.order-b.order;
            });
            for(var i in data) {
                    var completeDate = new Date(data[i].creationDate * 1000);
                    var day = completeDate.getDate();
                    var month = completeDate.getMonth();
                    var year = completeDate.getFullYear();
                    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var date = day + "-" + months[month] + "-" + year;
                    out_data = out_data + "<tr><td id='"+data[i].id+"'>" + counter + "</td><td><img class='icon' style='width: 50px;height: 50px' src='" + data[i].thumbnail + "' /></td><td style='cursor: pointer' >" +
                        data[i].name + "</td><td>" + date + "</td><td><i " +
                        " class='fa fa-edit' onclick=addNewWorkoutType('" + data[i].id + "') ></i>&nbsp;&nbsp;" +
                        "<i class='fa fa-trash' onclick=deleteWorkoutType('" + data[i].id + "')></i></td></tr>";
                    counter++;
                    total_records++;
                }
            $("#overlay").css("display", "none");
            out_data = out_data+'</tbody>';
            $("#catTable").html(out_data);
            $("#catTable").dataTable();
            $('.sortable').sortable({
                revert: true,
                connectWith: ".sortable",
                placeholder: "ui-state-highlight",
                helper: function(e, tr)
                {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                        // Set helper cell sizes to match the original sizes
                        $(this).width($originals.eq(index).width());
                    });
                    $helper.css("background-color", "rgb(223, 240, 249)");
                    return $helper;
                },
                stop: function (event, ui) {
                    console.log('order updated here----');
                    $('#catTable').find('tbody').find('tr').find('td').each(function () {

                        if ($(this).attr('id')) {
                            console.log($(this).attr('id'));
                            orders.push($(this).attr('id'));
                        }

                    });
                }
            });


        },6000);
    }
    else{
        alert('Invalid Param found');
        back();
    }
}


function addNewWorkoutType(parent_id) {
    if(parent_id == "0") {
        $(".modal-header").css({"display": "block"});
        $(".modal-title").html("Add Workout Video");
        $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
            "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
            "<label>Enter Video Title</label><input type='text' id='add_cat_name' value='' " +
            "placeholder='Enter video title' class='form-control' /></div><div class='col-md-3 form-group' ><label>Video File</label><input type='file'" +
            'id="videoImage"  style="margin-top:5px;" onchange=setCatImage1(this,"livepic") ' +
            "/><input type='hidden' value='' id='selectedvideofile' /> "+
            "</div></div><div class='row'>" +
            "<div class='col-md-6 form-group' ><label>Thumbnail</label><input type='file'" +
            "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
            "/><input type='hidden' value='' id='selectedfilename' /> <div class='photobox'>" +
            "<img src='images/img.png' class='livepic img-responsive' id='selectedImg'/></div>" +
            "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
            "style='text-align:right;margin-top:40px'><input type='button' " +
            "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
            "<input type='button' value='Add WorkOut' onclick=updateContent('0') id='addworkbtn' " +
            'class="formbtn btn btn-info" /> </div> </div> <img src="images/default.gif" class="loadNow" /> <video id="video" controls preload="none" width="640"  style="display:none" ' +
            'poster="http://media.w3.org/2010/05/sintel/poster.png"  >'+
            '</video><canvas id="canvas" style="display:none"></canvas>');
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show");

    }
    else{

        var newworkref = database.ref().child('WorkoutVideos');
        newworkref.orderByKey().equalTo(parent_id).on("value", function(snapshot) {
            // console.log(Object.keys(snapshot.val())[0].id);
            var obj = snapshot.val();
            for(var i in obj){
                var name =   obj[i].name;
                var image =  obj[i].thumbnail;
                var video =  obj[i].video;
                var id = obj[i].id;
                var path = video;
                    if(path.length>50)
                     path = path.substr(0,30)+"....";


                $(".modal-header").css({"display": "block"});
                $(".modal-title").html("Edit Workout");
                $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                    "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                    "<label>Enter Video Title</label><input type='text' id='add_cat_name' value='"+name+"' " +
                    "placeholder='Enter video title' class='form-control' /></div><div class='col-md-6 form-group' ><label>Video File</label><ul class='list-unstyled list-inline' id='filepath1' style='display:none' ><li><input type='file'" +
                    "id='videoImage' value='' onchange=setCatImage1(this,'livepic') style='margin-top: 5px;' " +
                    "/></li><li><i class='fa fa-trash' onclick=onFileSelected('filepath1')></i></li></ul><ul class='list-unstyled list-inline' id='filepath'><li><input type='text' id='file_txt' value='"+video+"' title='"+video+"' disabled/></li><li><i class='fa fa-edit' onclick=onFileSelected('filepath')></i></li>" +
                    "</ul><input type='hidden' value='' id='selectedvideofile' /> <input type='hidden' value='' id='selectedvideofile1' />"+
                    "</div></div><div class='row'>" +
                    "<div class='col-md-6 form-group' ><label>Thumbnail</label><input type='file'" +
                    "id='categoryImage' onchange=setCatImage(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
                    "/><input type='hidden' value='' id='selectedfilename' /> <div class='photobox'>" +
                    "<img src='"+image+"' class='livepic img-responsive' id='selectedImg'/></div>" +
                    "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' " +
                    "style='text-align:right;margin-top:40px'><input type='button' " +
                    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; " +
                    "<input type='button' value='Edit WorkOut' onclick=updateContent('"+id+"') id='addworkbtn' " +
                    'class="formbtn btn btn-info" /> </div> </div> <img src="images/default.gif" class="loadNow" /> <video id="video" controls preload="none" width="640"  style="display:none" ' +
                    'poster="http://media.w3.org/2010/05/sintel/poster.png"  >'+
                    '</video><canvas id="canvas" style="display:none"></canvas>');
                $(".modal-footer").css({"display": "none"});
                $("#myModal").modal("show");
                $('#selectedfilename').val(image);
                $('#selectedvideofile').val(video);
                $('#selectedvideofile1').val(video);
                // $('#videoImage').val(video);

            }
        });



    }
}

function onFileSelected(idd) {
    if(idd == 'filepath') {
        $('#filepath').css('display','none');
        $('#filepath1').css('display','block');

    }
    else{
        $('#filepath').css('display','block');
        $('#filepath1').css('display','none');
        $('#selectedvideofile').val($('#selectedvideofile1').val());
    }

}
function updateContent(parent_id)
{
    var video_image = $("#selectedfilename").val();
    var video_file = $("#selectedvideofile").val();

    var video_name = $("#add_cat_name").val();
    var creation_date = Math.round((new Date()).getTime() / 1000);

    var newworkref = database.ref().child('WorkoutVideos').push();
    var newworkrefkey = newworkref.key;

    if(parent_id == "0") {   // add code
        if(video_image == "" || video_file == "" || video_name == ''){
            $("#message").html("Please fill the required fields first");
            return false;
        }
        creation_date = Math.round((new Date()).getTime() / 1000);
        newworkref.set({
            creationDate : creation_date,
            id : newworkrefkey,
            thumbnail : video_image,
            name : video_name,
            parentId:getUrlParameter('id'),
            video:video_file,
            order:total_records,
            duration:""

        });
        $("#myModal").modal("hide");
        loadWorkout_Types();
    }
    else { // edit code...
        video_image = $("#selectedfilename").val();
        video_file = $("#selectedvideofile").val();

        video_name = $("#add_cat_name").val();

        if(video_image == "" || video_file == "" || video_name == '') {
            $("#message").html("Please Fill the Required Fields First");
            return false;
        }

        newworkref = database.ref().child('WorkoutVideos/'+parent_id);
        // newworkrefkey = newworkref.key;

        newworkref.update({
            thumbnail : video_image,
            name : video_name,
            parentId:getUrlParameter('id'),
            video:video_file
        });

        newworkref.on("value",function (snapshot) {
            console.log(JSON.stringify(snapshot.val())+" "+snapshot.val().name);
            if(snapshot.val().name == video_name) {
                setTimeout(function () {
                    loadWorkout_Types();
                    $("#myModal").modal("hide");

                },1000);

            }

        });


        // loadWorkouts();
    }
}


function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

/*function setCatImage(input,showClass) {
    var file = input.files[0];
    var reader = new FileReader();

    reader.addEventListener("load",function(e){
        console.log('files --'+JSON.stringify(file.pathname));
        var file_name = file.name;
        var ext = file_name.substr(file_name.lastIndexOf(".")+1,file_name.length);
        console.log(ext);
        if(!checkVideoType(ext.toUpperCase())) {
            $('#message').html("please select these "+files.join(",") +" video files");
        }

        else{
            /!*$('video').bind('video_really_ready', function() {
              VideoSnapper.captureAsCanvas($('#video'),100,function (load) {
                  console.log('load -- '+load);
              });
            });*!/
            console.log('path -- '+$('#categoryImage').val());

            // source_tag.src = URL.createObjectURL(file);
            // $("#video").html("<source src='"+URL.createObjectURL(file)+"' type='video/mp4'/>" );
            // document.getElementById("video").play();
            var video = document.getElementById('video');

            source.setAttribute('src', URL.createObjectURL(file));
            console.log($('#video').children().length);
            if($('#video').children().length == 0)
            video.appendChild(source);

            $('#video').attr({'autoplay':'true'});

            setTimeout(function () {
                var img = document.getElementById('selectedImg');
                img.src='';
                var scale = 0.25;
                var video = $("#video").get(0);
                var canvas = document.getElementById("canvas");
                canvas.width = video.videoWidth * scale;
                canvas.height = video.videoHeight * scale;
                canvas.getContext('2d')
                    .drawImage(video, 0, 0, canvas.width, canvas.height);
                console.log('class -- '+showClass);
                img.src = canvas.toDataURL();
                document.getElementById("video").pause();
                // document.getElementById("video").innerHTML = "";

            },2000);

            // document.getElementById("video").pause();



        }

    });
    reader.readAsDataURL(file);
}*/


function setCatImage(input,showClass) {

    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        $('.'+showClass).attr('src', e.target.result);
        var upload_pic = "file_"+new Date().getTime()+".jpg";
        writeDataImage(upload_pic);
        // var database = firebase.database();
        function writeDataImage(upload_pic) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: file.type
            };
            storageRef.child('images/' + upload_pic).put(file, metadata).then(function(snapshot){
                $("#addworkbtn").attr("disabled",false);

                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs) {
                        console.log('image upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedfilename").val(snapshot[i].downloadURLs);
                        $("#addworkbtn").attr("disabled",false);

                    }
                }
            });

        }
    };
    reader.readAsDataURL(file);
}



function setCatImage1(input) {

    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        $("#addworkbtn").attr("disabled",true);
        // $('.'+showClass).attr('src', e.target.result);
        var file_name = file.name;
        var ext = file_name.substr(file_name.lastIndexOf(".")+1,file_name.length);
        console.log(ext);
        if(!checkVideoType(ext.toUpperCase())) {
            $('#message').html("please select these "+files.join(",") +" video files");
            return ;
        }
        var upload_video = "video_"+new Date().getTime()+"."+ext.toLowerCase();
        $('.loadNow').css('display','block');
        writeDataImage(upload_video);
        // var database = firebase.database();
        function writeDataImage(upload_video) {
            var storageRef = firebase.storage().ref();
            var metadata = {
                contentType: ext
            };
            storageRef.child('videos/' + upload_video).put(file, metadata).then(function (snapshot) {
                $('.loadNow').css('display','none');
                for(var i in snapshot) {
                    // var download_url = snapshot[i].downloadURLs[0];

                    if(snapshot[i].downloadURLs){
                        console.log('video upload -- '+snapshot[i].downloadURLs);
                        // console.log(snapshot[i]);
                        $("#selectedvideofile").val(snapshot[i].downloadURLs);
                        $("#addworkbtn").attr("disabled",false);

                    }
                }
            });
        }
    };
    reader.readAsDataURL(file);
}

function deleteWorkoutType(work_id) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Workout</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+work_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(work_id) {

    var newworkref = database.ref().child('WorkoutVideos/'+work_id).remove().then(function(snapshot){
        loadWorkout_Types();
    }).catch(function (error) {
        showMessage(error.code,"red");
    });
}

function onOrderSave() {
    console.log(orders);
    if(orders.length>0) {
        for (var i = 0; i < orders.length; i++) {
            var newworkref = database.ref().child('WorkoutVideos/' + orders[i]);
            // newworkrefkey = newworkref.key;

            newworkref.update({
                order: i
            });

            if (i == orders.length - 1) {
                orders = [];
            }
        }
        showMessage1('Order saved successfully','Order','green');
    }
    else{
        showMessage1('No changes made yet','Order','red');

    }
}

loadWorkout_Types();