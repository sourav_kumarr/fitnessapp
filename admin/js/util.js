/**
 * Created by Sourav on 6/9/2017.
 */

var files = ["MP4","MOV","WMV","FLV","AVI"];

function validateNumberFloat(number) {
    var re = /^(?:\d{1,9})?(?:\.\d{1,9})?$/;
    return re.test(number);
}

function back(){
    window.history.back();
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function showMessage(message,color){
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:"+color+"'>"+message+"</span>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
    // setTimeout(stopMusic,2000);
}

function showMessage1(message,title,color){
    $(".modal-header").html('<h2>'+title+'</h2>');
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:"+color+"'>"+message+"</span>");
    $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
    $(".modal-footer").css('display','block');
    $("#myModal").modal("show");
    // setTimeout(stopMusic,2000);
}

function checkVideoType(file_type) {

    for(var i=0;i<files.length;i++) {
        if(file_type == files[i]) {
            return true;
        }
    }
    return false;
}

function getDateFromTimestamp(timestamp) {
    var completeDate = new Date(timestamp*1000);
    var day = completeDate.getDate();
    var month = completeDate.getMonth();
    var year = completeDate.getFullYear();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var date = day+"-"+months[month]+"-"+year;
    return date;
}