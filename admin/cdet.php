<?php
include('header.php');
$cat_id = $_REQUEST['_'];
echo "<input type='hidden' value=".$cat_id." id='cat_id' />";
?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
</style>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> Back <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li id="addcategorybtn"><a class="btn btn-info" onclick="addNewCategory('<?php echo $cat_id ?>')" style="color: white"><i class="fa fa-plus"></i> <i id="btn_name">Add New Sub-Category</i></a></li>
                        <li id="addproductbtn" style="display:none;"><a class="btn btn-info" onclick="addNewProduct('<?php echo $cat_id ?>')" style="color: white"><i class="fa fa-plus"></i> <i id="btn_name">Add New Product</i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        View "<span id="cat_name1"></span>" Detail
                    </p>
                </div>
            </div>
            <div class="col-md-8 boxes">
                <h2 style="text-align: center" id="mainDetail">Category Detail</h2>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-1 form-group">
                        <label>Image</label>
                        <img id="cat_image" src="images/user.png" class="img-responsive img-thumbnail" />
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Name</label>
                        <p id="cat_name"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Created On</label>
                        <p id="added_on"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Created By</label>
                        <p id="admin_name" style="text-transform: capitalize"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Level</label>
                        <p id="level"></p>
                    </div>
                    <div class="col-md-2 form-group">
                        <label>Approval</label>
                        <p id="approval"></p>
                    </div>

                </div>
                <div class="col-md-12" style="height:300px;overflow-y: scroll">
                    <hr>
                    <h4 style="text-align:center;" id="subtype">All Genres</h4>
                    <table class="table table-stripped table-bordered" id="sub_cat_table"></table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-12 boxes">
                    <h2 style="text-align: center">Approval Status</h2>
                    <hr>
                    <div class="col-md-12" style="margin-bottom: 20px" id="approvalTable"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(".loader").show();
    var cat_id = $("#cat_id").val();
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategory","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            var catData = data.catData;
            $("#cat_name1").html(catData.cat_name);
            $("#cat_name").html(catData.cat_name);
            $("#admin_name").html(catData.admin_name);
            var addedon = new Date(catData.added_on*1000);
            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
            $("#added_on").html(addedon.getDate()+" "+months[addedon.getMonth()]+", "+addedon.getFullYear());
            if (catData.cat_image != "") {
                $("#cat_image").attr("src", "api/Files/images/" + catData.cat_image);
            }
            $("#level").html(catData.cat_type);
            var approvalTable = "<table class='table table-bordered'><tr><td>S.No</td><td>Admin Name</td>" +
            "<td>Approval Status</td></tr>";
            for(var i=0;i<catData.approval.length;i++){
                if(catData.approval[i].admin_id == $("#admin_id").val()){
                    if(catData.approval[i].approval_status == "1"){
                        $("#approval").html("<td class='buttonsTd' data-title='Approval'><button type='button' " +
                        "class='btn btn-success' disabled ><i class='fa fa-check'></i></button>" +
                        "<button type='button' class='btn btn-danger' onclick=changeApproval('"+catData.approval[i].row_id+"','0') ><i class='fa fa-close'></i></button></td>")
                    }
                    else{
                        $("#approval").html("<td class='buttonsTd' data-title='Visibility'><button type='button' " +
                        "class='btn btn-success' onclick=changeApproval('"+catData.approval[i].row_id+"','1')><i class='fa fa-check'></i></button>" +
                        "<button type='button' class='btn btn-danger' disabled><i class='fa fa-close'></i></button></td>")
                    }
                }
                var textStatus = "<i class='fa fa-close' style='color:red'></i> Declined";
                if(catData.approval[i].approval_status == "1"){
                    textStatus = "<i class='fa fa-check' style='color:green'></i> Approved";
                }
                approvalTable += "<tr><td class='buttonsTd' data-title='S.No'>"+(i+1)+"</td><td class='buttonsTd' " +
                "data-title='Admin Name'>"+catData.approval[i].admin_name.charAt(0).toUpperCase()+
                catData.approval[i].admin_name.slice(1)+"</td><td class='buttonsTd' " +
                "data-title='Approval Status'>"+textStatus+"</td></tr>";
            }
            approvalTable += "</table>";
            $("#approvalTable").html(approvalTable);
        }
    });
    $.post(url,{"type":"isChildExist","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        if (status == "Success") {
            $.post(url,{"type":"getCategories"} ,function (data) {
                var status = data.Status;
                if (status == "Success") {
                    var catArray = data.data;
                    var sub_cat_table = "<tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                    "<th>Type</th><th>Approval</th><th>Action</th></tr>";
                    var j=0;
                    for(var i=0;i<catArray.length;i++){
                        if(catArray[i].parent_id == cat_id){
                            j++;
                            if(catArray[i].cat_type == "Sub Category"){
                                $("#subtype").html("All Sub Categories");
                            }
                            for(var k=0;k<catArray[i].approval.length;k++) {
                                if(catArray[i].approval[k].admin_id == $("#admin_id").val()) {
                                    if (catArray[i].approval[k].approval_status == "1") {
                                        var approvalbtns="<button type='button' class='btn btn-success' disabled >" +
                                            "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                            "onclick=changeApproval('"+catArray[i].approval[k].row_id+"','0') ><i class='fa fa-close'>" +
                                            "</i></button>";
                                    }else{
                                        var approvalbtns="<button type='button' class='btn btn-success' " +
                                            "onclick=changeApproval('"+catArray[i].approval[k].row_id+"','1')>" +
                                            "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                            "class='btn btn-danger'><i class='fa fa-close'></i></button>";
                                    }
                                }
                            }
                            var addedon = new Date(catArray[i].added_on*1000);
                            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                            addedon = addedon.getDate()+" "+months[addedon.getMonth()]+", "+addedon.getFullYear();
                            sub_cat_table+="<tr><td>"+j+"</td><td><img class='img-responsive img-thumbnail' " +
                            "style='max-height:35px' src='api/Files/images/"+catArray[i].cat_image+"'/></td>" +
                            "<td><a href='cdet?_="+catArray[i].cat_id+"'>"+catArray[i].cat_name+"</a></td>" +
                            "<td>"+addedon+"</td><td>"+catArray[i].admin_name.charAt(0).toUpperCase()+
                            catArray[i].admin_name.slice(1)+"</td><td>"+catArray[i].cat_type+"</td><td class='buttonsTd' " +
                            "data-title='Approval'>"+approvalbtns+"</td><td data-title='Action'><i class='fa fa-edit'" +
                            " onclick=editCategoryData('"+catArray[i].cat_id+"') style='cursor:pointer;color:#31B0D5'></i>" +
                            "&nbsp;&nbsp;<i class='fa fa-trash' onclick=deleteCategory('"+catArray[i].cat_id+"') " +
                            "style='color:#D05E61;cursor:pointer'></i></td></tr>";
                        }
                    }
                    if(j>0){
                        $("#sub_cat_table").html(sub_cat_table);
                    }else{
                        $("#sub_cat_table").html("<tr><td colspan='6' style='text-align: center'>Not Any Sub-Category" +
                            " Available Yet</td></tr>");
                    }
                }
                $(".loader").hide();
            });
        }else{
            $("#subtype").html("All Products");
            $("#mainDetail").html("Genre Detail");
            $("#addcategorybtn").css("display","none");
            $("#addproductbtn").css("display","block");
            var productUrl = "api/productProcess.php";
            $.post(productUrl,{"type":"getProductsOfCategory","cat_id":cat_id} ,function (data) {
                var status = data.Status;
                if (status == "Success") {
                    var ProductData = data.ProductData;
                    var product_table = "<tr><th>#</th><th>Image</th><th>Name</th><th>Added On</th><th>Added By</th>" +
                        "<th>Price</th><th>Approval</th><th>Action</th></tr>";
                    var j=0;
                    for(var i=0;i<ProductData.length;i++){
                        for(var k=0;k<ProductData[i].approval.length;k++) {
                            if(ProductData[i].approval[k].admin_id == $("#admin_id").val()) {
                                if (ProductData[i].approval[k].approval_status == "1") {
                                    var approvalbtns="<button type='button' class='btn btn-success' disabled >" +
                                    "<i class='fa fa-check'></i></button><button type='button' class='btn btn-danger' " +
                                    "onclick=changeApproval('"+ProductData[i].approval[k].row_id+"','0') ><i class='fa fa-close'>" +
                                    "</i></button>";
                                }else{
                                    var approvalbtns="<button type='button' class='btn btn-success' " +
                                    "onclick=changeApproval('"+ProductData[i].approval[k].row_id+"','1')>" +
                                    "<i class='fa fa-check'></i></button><button type='button' disabled " +
                                    "class='btn btn-danger'><i class='fa fa-close'></i></button>";
                                }
                            }
                        }
                        var addedon = new Date(ProductData[i].added_on*1000);
                        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                        addedon = addedon.getDate()+" "+months[addedon.getMonth()]+", "+addedon.getFullYear();
                        product_table+="<tr><td>"+(i+1)+"</td><td><img class='img-responsive img-thumbnail' " +
                        "style='max-height:35px' src='api/Files/images/"+ProductData[i].product_image+"'/></td>" +
                        "<td><a href='pdet?_="+ProductData[i].product_id+"'>"+ProductData[i].product_name+"</a></td>" +
                        "<td>"+addedon+"</td><td>"+ProductData[i].uploaded_by.charAt(0).toUpperCase()+ProductData[i].uploaded_by.slice(1)+" ( "+ProductData[i].user_name.charAt(0).toUpperCase()+
                        ProductData[i].user_name.slice(1)+" )</td><td>$"+ProductData[i].product_price+"</td><td class='buttonsTd' " +
                        "data-title='Approval'>"+approvalbtns+"</td><td data-title='Action'><i class='fa fa-edit'" +
                        " onclick=editProductData('"+ProductData[i].product_id+"') style='cursor:pointer;color:#31B0D5'></i>" +
                        "&nbsp;&nbsp;<i class='fa fa-trash' onclick=delete_product('"+ProductData[i].product_id+"') " +
                        "style='color:#D05E61;cursor:pointer'></i></td></tr>";
                    }
                }
                else{
                    product_table="<tr><td colspan='6' style='text-align: center'>Not Data Available Yet</td></tr>";
                }
                $("#sub_cat_table").html(product_table);
                $(".loader").hide();
            });
        }
    });

</script>
