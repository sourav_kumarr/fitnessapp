<?php
include('header.php');
include('api/Constants/configuration.php');
?>
<style>
    .icon {
        border: 2px solid #aaa;
        border-radius: 5px;
        height: 36px;
        max-width: 110px;
    }
    .workout{
        color:#e25050 !important;
        font-size: 18px;
    }
</style>
<!-- page content -->
<div id="overlay">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="list-unstyled list-inline">
                        <li class="back" onclick="back()"><i class="fa fa-arrow-left"></i></li>
                        <li><a href="index2.php"><span style="font-size: 18px;color: #80879c;">All Diet Plan's</span></a></li>
                        <li class="pull-right"><input type="button" value="+ Add DietPlan" class="btn btn-info" onclick="addNewWorkout('0')"/></li>
                        <li class="pull-right"><input type="button" value="Save Order" class="btn btn-info" onclick="onOrderSave()"/></li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        From here admin can manage/modify the content of the catalogs
                    </p>
                    <table id="catTable" class="table table-striped table-bordered display">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="file" value="" style="display: none" id="categoryImage" />
<div id="categoryImageDisplay"></div>

<?php
include('footer.php');
?>
<script src="js/dietplan.js"></script>

