<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class CATEGORY
{
    public $link = null;
    public $userClass = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addCategory($cat_name,$admin_id,$parent_id,$level)
    {
        $link = $this->link->connect();
        if ($link) {
            $file_response = $this->link->storeImage('cat_image', 'images');
            if ($file_response[STATUS] == Error) {
                $this->response[STATUS] = $file_response[STATUS];
                $this->response[MESSAGE] = $file_response[MESSAGE];
                return $this->response;
            }
            $file_name = $file_response['File_Name'];
            $query = "insert into categories(cat_name,cat_image,added_on,added_by,parent_id,level) VALUES 
            ('$cat_name','$file_name','$this->currentDateTimeStamp','$admin_id','$parent_id','$level')";
            $result = mysqli_query($link, $query);
            if($result) {
                $cat_id = $this->link->getLastId();
                $this->storeToApprovalTable('category',$cat_id,'0');
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Category Added SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCategory($cat_name,$cat_parent,$cat_id,$imageChanged,$level,$admin_id)
    {
        $link = $this->link->connect();
        if ($link) {
            if($imageChanged == "yes"){
                $file_response = $this->link->storeImage('cat_image','images');
                if ($file_response[STATUS] == Error) {
                    $this->response[STATUS] = $file_response[STATUS];
                    $this->response[MESSAGE] = $file_response[MESSAGE];
                    return $this->response;
                }
                $file_name = $file_response['File_Name'];
                $query = "update categories set cat_name='$cat_name', parent_id='$cat_parent', cat_image='$file_name'
                ,level='$level', added_by='$admin_id',added_on = '$this->currentDateTimeStamp' where cat_id = '$cat_id'";
            }
            else{
                $query = "update categories set cat_name='$cat_name', parent_id='$cat_parent',level='$level', 
                added_by='$admin_id',added_on = '$this->currentDateTimeStamp' where cat_id = '$cat_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Category Updated SuccessFully";
                $this->response['catId'] = $cat_id;
                ($adminResponse = ($this->getAllAdmins()));
                if($adminResponse[STATUS] == Success){
                    for($i=0;$i<count($adminResponse['userData']);$i++){
                        $query = "update approvals set approval_status='0' where element_type = 'category' 
                        and element_id = '$cat_id'";
                        $result = mysqli_query($link,$query);
                        if(!$result){
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCategoryExistence($cat_name,$parent_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from categories where cat_name = '$cat_name' and parent_id='$parent_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Category Name Already Existance Please Use Diffrent One";
                    $row = mysqli_fetch_array($result);
                    $this->response['catId'] = $row['cat_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function isChildExist($cat_id){
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories where parent_id = '$cat_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Child Exist";
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Child Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllCategories()
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories order by cat_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($catData = mysqli_fetch_array($result)) {
                        $cat_id = $catData['cat_id'];
                        $topParent = $this->getTopParentName($cat_id);
                        $temp = $this->getApprovalStatus('category',$cat_id);
                        $approval = array();
                        if($temp[STATUS] == Success) {
                            $approval = $temp['approval'];
                        }
                        $admin_id = $catData['added_by'];
                        $userresponse = $this->userClass->getParticularUserData($admin_id);
                        $userData = $userresponse['UserData'];
                        $catArray[]=array(
                            "cat_id"=>$catData['cat_id'],
                            "cat_name"=>$catData['cat_name'],
                            "cat_image"=>$catData['cat_image'],
                            "added_on"=>$catData['added_on'],
                            "added_by"=>$catData['added_by'],
                            "admin_name"=>$userData['user_name'],
                            "parent_id"=>$catData['parent_id'],
                            "level"=>$catData['level'],
                            "cat_type"=>$catData['cat_type'],
                            "topParent"=>$topParent,
                            "approval"=>$approval
                        );

                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $catArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Categories Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularCatData($catId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories where cat_id='$catId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $catData = mysqli_fetch_assoc($result);
                    $cat_id = $catData['cat_id'];
                    $temp = $this->getApprovalStatus('category',$cat_id);
                    $approval = array();
                    if($temp[STATUS] == Success) {
                        $approval = $temp['approval'];
                    }
                    $admin_id = $catData['added_by'];
                    $userresponse = $this->userClass->getParticularUserData($admin_id);
                    $userData = $userresponse['UserData'];
                    $catArray = array(
                        "cat_id"=>$catData['cat_id'],
                        "cat_name"=>$catData['cat_name'],
                        "cat_image"=>$catData['cat_image'],
                        "added_on"=>$catData['added_on'],
                        "added_by"=>$catData['added_by'],
                        "admin_name"=>$userData['user_name'],
                        "parent_id"=>$catData['parent_id'],
                        "level"=>$catData['level'],
                        "cat_type"=>$catData['cat_type'],
                        "approval"=>$approval
                    );
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Category Exist";
                    $this->response['catData'] = $catArray;
                    unset($this->response['approval']);
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Category";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getTopParentName($catId)
    {
        $temp =$this->getParticularCatData($catId);
        $level = $temp['catData']['level'];
        if($level>0){
            $newCat_id = $temp['catData']['parent_id'];
            return $this->getTopParentName($newCat_id);
        }else{
            $cat_name = $temp['catData']['cat_name'];
        }
        return $cat_name;
    }
    public function storeToApprovalTable($type,$element_id,$approval_status){
        $link = $this->link->connect();
        if($link) {
            $response = $this->getAllAdmins();
            if($response[STATUS] == Error){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $response[MESSAGE];
            }else{
                $adminData = $response['userData'];
                for($i=0;$i<count($adminData);$i++){
                    $admin_id = $adminData[$i]['admin_id'];
                    $query = "insert into approvals(element_type,element_id,approval_status,admin_id) values 
                    ('$type','$element_id','$approval_status','$admin_id')";
                    $result = mysqli_query($link,$query);
                    if($result){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Approval Added";
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllAdmins(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_type = 'admin'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $userData[] = array(
                            "admin_id"=>$rows['user_id'],
                            "admin_name"=>$rows['user_name'],
                            "admin_email"=>$rows['user_email']
                        );
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Admin Data Found";
                        $this->response['userData'] = $userData;
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No any Found Yet";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getApprovalStatus($element_type,$element_id){
        $approval = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from approvals where element_type = '$element_type' and element_id = '$element_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($approvalData = mysqli_fetch_array($result)) {
                        $admin_id = $approvalData['admin_id'];
                        $temp = $this->userClass->getParticularUserData($admin_id);
                        $admin_name = $temp['UserData']['user_name'];
                        $approval[]=array(
                            "row_id"=>$approvalData['row_id'],
                            "element_type"=>$approvalData['element_type'],
                            "element_id"=>$approvalData['element_id'],
                            "approval_status"=>$approvalData['approval_status'],
                            "admin_id"=>$approvalData['admin_id'],
                            "admin_name"=>$admin_name
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Approval Found";
                    $this->response['approval'] = $approval;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Approval Data Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function changeApproval($approval_id, $approval_status)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update approvals set approval_status = '$approval_status' where row_id ='$approval_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Status Has Been Changed Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteCategory($cat_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from categories WHERE cat_id='$cat_id'");
                    $leupdate   = mysqli_query($link, "delete from approvals WHERE element_id='$cat_id' AND element_type='category'");
                    if ($update and $leupdate) {
                        $row = mysqli_fetch_array($result);
                        unlink("Files/images/".$row['cat_image']);
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Category Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}