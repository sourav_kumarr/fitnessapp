<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/21/2017
 * Time: 5:36 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
require_once('CATEGORY.php');
class PRODUCTS
{
    public $userClass = null;
    public $category = null;
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->category = new CATEGORY();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addProduct($cat_id,$prod_name,$prod_desc,$file_type,$product_price,$uploaded_by,$user_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $file_response = $this->link->storeImage('product_file',$file_type);
            if ($file_response[STATUS] == Error) {

                $this->response[STATUS] = $file_response[STATUS];
                $this->response[MESSAGE] = $file_response[MESSAGE];
                return $this->response;
            }
            $file_name = $file_response['File_Name'];
            $image_response = $this->link->storeImage('product_image','image');
            if ($image_response[STATUS] == Error) {
                $this->response[STATUS] = $image_response[STATUS];
                $this->response[MESSAGE] = $image_response[MESSAGE];
                return $this->response;
            }
            $image_file = $image_response['File_Name'];
            $query = "insert into products (product_name,product_image,product_desc,file_type,category_id,file_name,
            product_price,likes,dislikes,downloads,playtime,uploaded_by,user_id,added_on) VALUES ('$prod_name','$image_file',
            '$prod_desc','$file_type','$cat_id','$file_name','$product_price','0','0','0','0','$uploaded_by','$user_id','$this->currentDateTimeStamp')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $product_id = $this->link->getLastId();
                $this->category->storeToApprovalTable('product',$product_id,'0');
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Product Added SuccessFully";
                $this->response['productId'] = $product_id;

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editProduct($prod_id,$imageChanged,$fileChanged,$file_type,$prod_name,$prod_desc,$product_price,$uploaded_by,$user_id)
    {
        $link = $this->link->connect();
        if ($link) {
            if($imageChanged == "yes") {
                $image_response = $this->link->storeImage('product_image', 'image');
                if ($image_response[STATUS] == Error) {
                    $this->response[STATUS] = $image_response[STATUS];
                    $this->response[MESSAGE] = $image_response[MESSAGE];
                    return $this->response;
                }
                $image_file = $image_response['File_Name'];
            }
            if($fileChanged == "yes") {
                $file_response = $this->link->storeImage('product_file',$file_type);
                if ($file_response[STATUS] == Error) {
                    $this->response[STATUS] = $file_response[STATUS];
                    $this->response[MESSAGE] = $file_response[MESSAGE];
                    return $this->response;
                }
                $file_name = $file_response['File_Name'];
            }
            if($imageChanged == "yes" && $fileChanged == "yes"){
                $query = "update products set product_name='$prod_name',product_desc='$prod_desc',
                product_price='$product_price',uploaded_by='$uploaded_by',user_id='$user_id',
                product_image='$image_file',file_name='$file_name' where product_id = '$prod_id'";
            }
            else if($imageChanged == "yes" && $fileChanged == "no"){
                $query = "update products set product_name='$prod_name',product_desc='$prod_desc',
                product_price='$product_price',uploaded_by='$uploaded_by',user_id='$user_id',
                product_image='$image_file' where product_id = '$prod_id'";
            }
            else if($imageChanged == "no" && $fileChanged == "yes"){
                $query = "update products set product_name='$prod_name',product_desc='$prod_desc',
                product_price='$product_price',uploaded_by='$uploaded_by',user_id='$user_id',
                file_name='$file_name' where product_id = '$prod_id'";
            }
            else {
                $query = "update products set product_name='$prod_name',product_desc='$prod_desc',
                product_price='$product_price',uploaded_by='$uploaded_by',user_id='$user_id'
                where product_id = '$prod_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Product Detail Updated SuccessFully";
                $this->response['productId'] = $prod_id;
                ($adminResponse = ($this->category->getAllAdmins()));
                if($adminResponse[STATUS] == Success){
                    for($i=0;$i<count($adminResponse['userData']);$i++){
                        $query = "update approvals set approval_status='0' where element_type = 'product' 
                        and element_id = '$prod_id'";
                        $result = mysqli_query($link,$query);
                        if(!$result){
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkProductExistence($product_name, $cat_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from products where category_id = '$cat_id' and product_name = '$product_name'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Product With Same Name is Already Exist Please Enter Diffrent Name";
                    $row = mysqli_fetch_array($result);
                    $this->response['productId'] = $row['product_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularProductData($productId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from products where product_id='$productId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    ($productData = mysqli_fetch_assoc($result));
                    $category_id = $productData['category_id'];
                    ($catData = $this->category->getParticularCatData($category_id));
                    $user_id = $productData['user_id'];
                    ($userData = $this->userClass->getParticularUserData($user_id));
                    $temp = $this->category->getApprovalStatus('product',$productData['product_id']);
                    $approval = array();
                    if($temp[STATUS] == Success) {
                        $approval = $temp['approval'];
                    }
                    $commentResponse = $this->getComment($productData['product_id']);
                    $commentData = array();
                    if($commentResponse[STATUS] == Success) {
                        $commentData = $commentResponse['commentData'];
                    }
                    $counterResponse = $this->getCounter($productData['product_id']);
                    if($counterResponse[STATUS] == Success){
                        $counter = $counterResponse['Counter'];
                        $likes = $counter['likes'];
                        $dislikes = $counter['dislikes'];
                        $downloads = $counter['downloads'];
                        $playtime = $counter['playtime'];
                    }else{
                        $likes = 0;
                        $dislikes = 0;
                        $downloads = 0;
                        $playtime = 0;
                    }
                    $productArray=array(
                        "product_id"=>$productData['product_id'],
                        "product_name"=>$productData['product_name'],
                        "product_image"=>$productData['product_image'],
                        "product_desc"=>$productData['product_desc'],
                        "category_id"=>$productData['category_id'],
                        "category_name"=>$catData['catData']['cat_name'],
                        "file_name"=>$productData['file_name'],
                        "file_type"=>$productData['file_type'],
                        "product_price"=>$productData['product_price'],
                        "likes"=>$likes,
                        "dislikes"=>$dislikes,
                        "downloads"=>$downloads,
                        "playtime"=>$playtime,
                        "uploaded_by"=>$productData['uploaded_by'],
                        "user_id"=>$productData['user_id'],
                        "added_on"=>$productData['added_on'],
                        "user_name"=>$userData['UserData']['user_name'],
                        "approval"=>$approval,
                        "comments"=>$commentData
                    );
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Product Exist";
                    $this->response[BaseURLKey] = BaseURL;
                    $this->response['ProductData'] = $productArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Product Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getCategoryAllProducts($cat_id)
    {
        $productArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from products where category_id = '$cat_id' order by product_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($productData = mysqli_fetch_array($result)) {
                        $category_id = $productData['category_id'];
                        ($catData = $this->category->getParticularCatData($category_id));
                        $user_id = $productData['user_id'];
                        ($userData = $this->userClass->getParticularUserData($user_id));
                        $temp = $this->category->getApprovalStatus('product',$productData['product_id']);
                        $approval = array();
                        if($temp[STATUS] == Success) {
                            $approval = $temp['approval'];
                        }
                        $commentResponse = $this->getComment($productData['product_id']);
                        $commentData = array();
                        if($commentResponse[STATUS] == Success) {
                            $commentData = $commentResponse['commentData'];
                        }
                        $counterResponse = $this->getCounter($productData['product_id']);
                        if($counterResponse[STATUS] == Success){
                            $counter = $counterResponse['Counter'];
                            $likes = $counter['likes'];
                            $dislikes = $counter['dislikes'];
                            $downloads = $counter['downloads'];
                            $playtime = $counter['playtime'];
                        }else{
                            $likes = 0;
                            $dislikes = 0;
                            $downloads = 0;
                            $playtime = 0;
                        }
                        $productArray[]=array(
                            "product_id"=>$productData['product_id'],
                            "product_name"=>$productData['product_name'],
                            "product_image"=>$productData['product_image'],
                            "product_desc"=>$productData['product_desc'],
                            "category_id"=>$productData['category_id'],
                            "category_name"=>$catData['catData']['cat_name'],
                            "file_name"=>$productData['file_name'],
                            "file_type"=>$productData['file_type'],
                            "product_price"=>$productData['product_price'],
                            "likes"=>$likes,
                            "dislikes"=>$dislikes,
                            "downloads"=>$downloads,
                            "playtime"=>$playtime,
                            "uploaded_by"=>$productData['uploaded_by'],
                            "user_id"=>$productData['user_id'],
                            "added_on"=>$productData['added_on'],
                            "user_name"=>$userData['UserData']['user_name'],
                            "approval"=>$approval,
                            "comments"=>$commentData
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response[BaseURLKey] = BaseURL;
                    $this->response['ProductData'] = $productArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Product Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function deleteProduct($product_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from products where product_id='$product_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from products WHERE product_id='$product_id'");
                    $leupdate   = mysqli_query($link, "delete from approvals WHERE element_id='$product_id' AND element_type='product'");
                    if ($update && $leupdate) {
                        $row = mysqli_fetch_array($result);
                        if($row['file_name'] != "") {
                            $file_type = $row['file_type'];
                            if ($file_type == "Music") {
                                unlink("Files/audio/" . $row['file_name']);
                            } else if ($file_type == "Video") {
                                unlink("Files/video/" . $row['file_name']);
                            } else if ($file_type == "Literature") {
                                unlink("Files/literature/" . $row['file_name']);
                            } else if ($file_type == "Art Work") {
                                unlink("Files/art/" . $row['file_name']);
                            }
                        }
                        if($row['product_image'] != "") {
                            unlink("Files/images/" . $row['product_image']);
                        }
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Product Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function addComment($comment_text,$product_id,$user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "insert into comment_data(comment_text,user_id,comment_on,product_id) values ('$comment_text'
            ,'$user_id','$this->currentDateTimeStamp','$product_id')";
            $result = mysqli_query($link,$query);
            if($result){
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Comment Added Successfully";
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getComment($product_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from comment_data where product_id='$product_id' order by comment_id DESC";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $user_id = $rows['user_id'];
                        $userResponse = $this->userClass->getParticularUserData($user_id);
                        if($userResponse[STATUS] == Success) {
                            $userData = $userResponse['UserData'];
                            $commentData[] = array(
                                "comment_id" => $rows['comment_id'],
                                "comment_text" => $rows['comment_text'],
                                "user_id" => $rows['user_id'],
                                "user_name" => $userData['user_name'],
                                "user_profile" => $userData['user_profile'],
                                "register_source" => $userData['register_source'],
                                "comment_on" => $rows['comment_on'],
                                "product_id" => $rows['product_id']
                            );
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Comments Found";
                    $this->response['commentData'] = $commentData;
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Not any Comment Added Yet";
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function deleteComment($comment_id){
        $link = $this->link->connect();
        if($link) {
            $query = "delete from comment_data where comment_id = '$comment_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Comment Deleted Successfully";
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function counterUpdater($user_id,$product_id,$counter_type){
        $link = $this->link->connect();
        if($link) {
            if($counter_type == "likes") {
                $query = "select * from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='likes'";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $num = mysqli_num_rows($result);
                    if($num>0){
                        $query = "delete from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='likes'";
                        $result = mysqli_query($link, $query);
                        if($result){
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Unliked Successfully";
                        }
                        else{
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }else{
                        $query = "select * from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='dislikes'";
                        $result = mysqli_query($link, $query);
                        if($result) {
                            $num = mysqli_num_rows($result);
                            if($num>0){
                                $query = "delete from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='dislikes'";
                                $query2 = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','likes')";
                                $result = mysqli_query($link, $query);
                                $result2 = mysqli_query($link, $query2);
                                if ($result && $result2) {
                                    $this->response[STATUS] = Success;
                                    $this->response[MESSAGE] = "Liked Successfully";
                                } else {
                                    $this->response[STATUS] = Error;
                                    $this->response[MESSAGE] = $this->link->sqlError();
                                }
                            }else {
                                $query = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','likes')";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $this->response[STATUS] = Success;
                                    $this->response[MESSAGE] = "Liked Successfully";
                                } else {
                                    $this->response[STATUS] = Error;
                                    $this->response[MESSAGE] = $this->link->sqlError();
                                }
                            }
                        }
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }else if($counter_type == "dislikes") {
                $query = "select * from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='dislikes'";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $num = mysqli_num_rows($result);
                    if($num>0){
                        $query = "delete from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='dislikes'";
                        $result = mysqli_query($link, $query);
                        if($result){
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Dislike Removed Successfully";
                        }
                        else{
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }else{
                        $query = "select * from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='likes'";
                        $result = mysqli_query($link, $query);
                        if($result) {
                            $num = mysqli_num_rows($result);
                            if($num>0){
                                $query = "delete from counter where product_id = '$product_id' and user_id = '$user_id' and counter_type='likes'";
                                $query2 = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','dislikes')";
                                $result = mysqli_query($link, $query);
                                $result2 = mysqli_query($link, $query2);
                                if ($result && $result2) {
                                    $this->response[STATUS] = Success;
                                    $this->response[MESSAGE] = "Disliked Successfully";
                                } else {
                                    $this->response[STATUS] = Error;
                                    $this->response[MESSAGE] = $this->link->sqlError();
                                }
                            }else {
                                $query = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','dislikes')";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $this->response[STATUS] = Success;
                                    $this->response[MESSAGE] = "Disliked Successfully";
                                } else {
                                    $this->response[STATUS] = Error;
                                    $this->response[MESSAGE] = $this->link->sqlError();
                                }
                            }
                        }
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }else if($counter_type == "downloads") {
                $query = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','downloads')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Download Incremented Successfully";
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }else if($counter_type == "playtime") {
                $query = "insert into counter (product_id,user_id,counter_type) values ('$product_id','$user_id','playtime')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "PlayTime Incremented Successfully";
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
            $counter = array();
            $counterResponse = $this->getCounter($product_id);
            if($counterResponse[STATUS] == Success){
                $counter = $counterResponse['Counter'];
            }
            $this->response['Counter'] = $counter;
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getCounter($product_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from counter where product_id = '$product_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $likes = 0;
                    $dislikes = 0;
                    $downloads = 0;
                    $play_time = 0;
                    while($rows = mysqli_fetch_array($result)){
                        if($rows['counter_type'] == 'likes'){
                            $likes = $likes+1;
                        }else if($rows['counter_type'] == 'dislikes'){
                            $dislikes = $dislikes+1;
                        }else if($rows['counter_type'] == 'downloads'){
                            $downloads = $downloads+1;
                        }else if($rows['counter_type'] == 'playtime'){
                            $play_time = $play_time+1;
                        }
                    }
                    $counter = array("likes"=>$likes,"dislikes"=>$dislikes,"downloads"=>$downloads,"playtime"=>$play_time);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Counter'] = $counter;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response['Count'] = 0;
        }
        return $this->response;
    }
    public function totalproducts($file_type){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from products where file_type = '$file_type'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Count Found";
                $this->response['Count'] = $num;
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response['Count'] = 0;
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response['Count'] = 0;
        }
        return $this->response;
    }
    public function getPaymentStatus($product_id,$user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from purchased_record where item_id = '$product_id' and user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Purchased";
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Not Purchased";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}