    </div>
</div>
<!-- footer content -->
<footer>
    <div class="pull-right">
<!--        FitnessApp - Developed by <a href="http://sachtechsolution.com">SachTech Solution Pvt. Ltd.</a>-->
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
<input type="hidden" value="" id="limit_"/>
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/bootstrap-toggle.min.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.dataTables.min.js"></script>

<script src="https://www.gstatic.com/firebasejs/live/3.7/firebase.js"></script>
<script src="https://cdn.firebase.com/libs/firebaseui/1.0.1/firebaseui.js"></script>

    <script src="js/util.js"></script>
    <script src="js/db.js"></script>
    <script>
        // user count
        var query = database.ref("Users").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#userCount").html(users.numChildren());
        });
        // diet count
        query = database.ref("Workouts").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#workoutCount").html(users.numChildren());
        });

        query = database.ref("DietPlan").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#dietplanCount").html(users.numChildren());
        });

        query = database.ref("Diets").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#dietsCount").html(users.numChildren());
        });

        query = database.ref("DietRecipe").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#dietrecipeCount").html(users.numChildren());
        });

        query = database.ref("WorkoutVideos").orderByKey();
        query.once("value").then(function(users) {
//            console.log('users length -- '+users.numChildren());
            $("#workoutVideoCount").html(users.numChildren());
        });

        query = database.ref("FreeItemLimit").orderByKey();
        query.once("value").then(function(limit){
            console.log('limit here --- '+limit.val());
            $("#limit").html(" ("+limit.val()+")");
            $("#limit_").val(limit.val());
        });
        
        function onFreeLimit(ref) {
            $(ref).removeClass('active');
            console.log('onFreeLimit is called');
            var value = $("#limit_").val();
            $(".modal-title").html("<label style='color:#c9302c'>Free Limit</label>");
            $(".modal-body").html("<div class='row'><p id='message' style='color: red;font-weight: bold;margin-left: 10px'></p><div class='col-md-12'><label>Enter Limit</label></div>" +
                "<div class='col-md-12'><input type='number' id='limits' class='form-control' placeholder='Define your limit' value='"+value+"'/></div></div>");
            $(".modal-footer").html('<button type="button" class="btn btn-danger" onclick="onLimitSubmit()">Submit</button>' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $("#myModal").modal("show");
        }
        
        function onLimitSubmit() {
            var limit = $("#limits").val();
            if(limit.length == 0) {
                $("#message").html("Please enter limit");
                return false;
            }
            else if(limit<0) {
                $("#message").html("Limit should not be less than 0 ");
                return false;
            }
            else{

                var newworkref = database.ref().child('FreeItemLimit');
//                var newworkrefkey = newworkref.key;

                 newworkref.set(parseInt(limit));
                $("#message").html("Limit updated successfully ! ! ! ! !");
                $("#message").css("color","green");
                setTimeout(function () {
                    $("#limit").html(" ("+limit+")");
                    $('#myModal').modal('hide');
                },2000);
                $("#limit_").val(limit);
            }
            console.log('limit --- '+limit);
        }
    </script>

</body>
</html>
