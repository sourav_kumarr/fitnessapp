<?php
include('header.php');
?>

<!-- page content -->
<div id="overlay">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Users <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <!--<button onclick="window.location='api/excelProcess.php?dataType=allUsers'" class="btn btn-info btn-sm">Download Excel File</button>-->
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table id="catTable" class="table table-striped table-bordered">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script src="js/users.js"></script>