<!Doctype html>
<?php
    session_start();
    if(!(isset($_SESSION['userId']) && isset($_SESSION['userEmail']))){
        header("Location:login.php");
        return false;
    }
    echo "<input type='hidden' id='admin_id' value='".$_SESSION['userId']."' />";
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin || Fitness App</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">

    <style>
       /* #catTable_filter{
            display: none;
        }*/
        .icon {
            border: 2px solid #aaa;
            border-radius: 5px;
            height: 36px;
            max-width: 110px;
        }
    </style>
</head>
<body class="nav-md">
<div class="loader" style="display: none">
    <img class="loader_img" src="images/mini.png" />
</div>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index2.php" class="site_title"><span style="color: #e25050;font-family: cursive">FitnessApp</span></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix" style="border-bottom: 1px #e25050 solid">
                    <!--<div class="profile_pic">
                        <img src="images/img.png" alt="..." class="img-circle profile_img">
                    </div>-->
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2 style="font-family: cursive"><?php echo ucfirst($_SESSION['userEmail']) ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="index.php" id="index"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="index2.php" id="index2"><i class="fa fa-heartbeat"></i> Workout's</a></li>
                            <li><a href="dietplan" id="index"><i class="fa fa-cutlery"></i> Diet Plan's</a></li>
                            <li><a href="tips" id="index"><i class="fa fa-cloud"></i> Fitness Tip's</a></li>
                            <li><a href="users" id="users"><i class="fa fa-user"></i> Users</a></li>
                            <li onclick="onFreeLimit(this)"><a style="cursor: pointer" id="users"><i class="fa fa-tags"></i> Free Limit <span id="limit"></span></a></li>

                            <!--                            <li><a href="admins" id="admins"><i class="fa fa-users"></i>Administrators</a></li>-->
<!--                            <li><a href="orders" id="orders"><i class="fa fa-first-order"></i>Order Management</a></li>-->
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.png" alt=""><?php echo $_SESSION['userEmail'] ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a onclick="logout()"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                <!--<li><a onclick=changeAdminPassword('<?php /*echo $_SESSION['userId'] */?>')><i class="fa fa-lock pull-right"></i> Change Password</a></li>-->
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
        <div class="right_col" role="main">
            <div class="row tile_count">
                <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
                        <div class="count" id="userCount"></div>
                        <span class="count_bottom"><i class="green">Click </i>to Expand</span>
                    </div></a>
                <a href="index2.php"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-heartbeat"></i> Total Workout's</span>
                        <div class="count" id="workoutCount"></div>
                        <span class="count_bottom"><i class="green"></i> in Workout Categories</span>
                    </div></a>
                <a href="index2.php"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-video-camera"></i> Total Workout videos's</span>
                        <div class="count" id="workoutVideoCount"></div>
                        <span class="count_bottom"><i class="green"></i> in Workout Categories</span>
                    </div></a>
                <a href="dietplan"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-bar-chart"></i> Total Diet Plan's</span>
                        <div class="count" id="dietplanCount"></div>
                        <span class="count_bottom"><i class="green"></i> in Category</span>
                    </div>
                </a>

                <a href="dietplan"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"> <i class="fa fa-cutlery "></i> Total Diet's</span>
                        <div class="count" id="dietsCount"></div>
                        <span class="count_bottom"><i class="green"></i> in DietPlan Category</span>
                    </div>
                </a>

                <a href="dietplan"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-video-camera"></i> Total Diet Recipe</span>
                        <div class="count" id="dietrecipeCount"></div>
                        <span class="count_bottom"><i class="green"></i> in DietPlan Category</span>
                    </div>
                </a>
            </div>
            <script>
                function logout(){
                    window.location = 'logout.php';
                }
            </script>

