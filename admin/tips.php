<?php
include('header.php');
?>

<!-- page content -->
<div id="overlay">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All Fitness Tip's <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li class="pull-right"><input type="button" value="+ Add Fitness Tips" class="btn btn-info" onclick="addNewTip('0')"/></li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table id="catTable" class="table table-striped table-bordered">

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script src="js/tips.js"></script>