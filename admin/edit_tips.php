<?php
include('header.php');
include('api/Constants/configuration.php');
?>
<style>
    .icon {
        border: 2px solid #aaa;
        border-radius: 5px;
        height: 36px;
        max-width: 110px;
    }
    .workout{
        color:#e25050 !important;
        font-size: 18px;
    }
</style>
<!-- page content -->
<div id="overlay">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <ul class="list-unstyled list-inline">
                        <li class="back" onclick="back()"><i class="fa fa-arrow-left"></i></li>
                        <li><a href="index2.php"><span style="font-size: 18px;color: #80879c;">Edit FitnessTip</span></a></li>
                        <li class="pull-right"><button class="btn btn-danger" id="save_btn" onclick="onSaveItem()">Save</button></li>

                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        From here admin can manage/modify the content of the catalogs
                    </p>
                    <div class="row">
                        <input type='hidden' value='' id='selectedvideofile' />
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li><label>Fitness Title</label></li>
                                <li><input type="text" id="title" class="form-control" placeholder="Enter fitness title" style="margin-top: 16px"></li>
                                <li ><p id="message" style="color: red;margin-top: 2%"></p></li>
                            </ul>
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            <ul class="list-unstyled list-inline">
                                <li><label>Fitness Tips</label></li>
                                <li class="pull-right"><button onclick="onAddItem('ingredients')">+ Add More</button></li>
                            </ul>
                            <ul class="list-unstyled list-inline" id="ingredients_data" style="overflow: auto;height:200px">

                            </ul>
                        </div>
                    </div>

                    <!--<div class="row">
                        <div class="col-md-12">
                            <ul class="list-unstyled list-inline">
                                <li ><p id="message" style="color: red"></p></li>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<input type="file" value="" style="display: none" id="categoryImage" />
<div id="categoryImageDisplay"></div>

<?php
include('footer.php');
?>
<script src="js/edit_tips.js"></script>

